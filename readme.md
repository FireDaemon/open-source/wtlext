Abstract
========
wtlext is a header-only C++ library in the spirit of ATL and WTL (Active Template Library and Windows Template Library), providing functionality not found in WTL.

The code inherits WTL's Microsoft Public license and encourages both commercial and non-commercial use.

All components (i.e. classes, functions, ...) were designed with the goal in mind of possibly integrating them into WTL itself.  
Even if they are deemed not be generic or widely used enough, they are useful nevertheless in their own respect and complement WTL in a familiar way.


Components
==========

Currently, the following components are available:

* Working with __SAFEARRAY__
  ----

  Specialisations of `ATL::CComVariant` and a veneer to facilitate attaching (i.e. moving) `SAFEARRAY`.
* Encoding functions
  ----

  `AtlPercentEncode`+`AtlPercentDecode` are similar to AtlEscapeUrl, but not tied to URLs and additionally provide the possibility to
  specify a set of ASCII characters to escape.
* __CEventSource__
  ----

  `CEventSource` wraps reporting to the application eventlog (wrapping `RegisterEventSourceW`+`ReportEvent` API).
  
  It properly __xml-escapes__ the source name, and deduces event category and type from event id.
* __CSecurityPolicy__
  ----

  `CSecurityPolicy` wraps the `LsaAddAccountRights`+`LsaRemoveAccountRights` API.
* __CSettingsStore__
  ----

  MFC-like settings store functionality.
  (see also discussion https://sourceforge.net/p/wtl/discussion/374433/thread/b531e3bf/)
* __CStdStream__
  ----

  `ISequentialStream`+`IStream` implementation for STD_OUTPUT_HANDLE/STD_INPUT_HANDLE or pipes.
* __CFileStream__
  ----

  `ISequentialStream`+`IStream` implementation for reading from/writing to files.
  
  Of course there is SHCreateStreamOnFileEx but it can't handle long file paths (prefixed with "\\?\"), plus there's one less dependency.
* __CUserObjectSecurityDesc__
  ----

  Deriving from `ATL::CSecurityDesc`, `CUserObjectSecurityDesc` wraps the `GetUserObjectSecurity`+`SetUserObjectSecurity` API.
* __CInternetSession__, CHttpConnection, CHttpFile
  ----

  MFC-like wininet functionality.
  (see also discussion https://sourceforge.net/p/wtl/discussion/374433/thread/aa62a8f3/)
* __CTabListBox__, CTabListImpl
  ----

  `CTabListBox` is a listbox/combobox control that presents its items as tabs.

  ![Image](docs/img/CTabListBox.png?raw=true)
* __CSectionGroupBox__
  ----

  `CSectionGroupBox` is a Win32 BUTTON with BS_GROUPBOX style, drawing its caption semibold surrounded by only a light horizontal line.

  ![Image](docs/img/CSectionGroupBox.png?raw=true)