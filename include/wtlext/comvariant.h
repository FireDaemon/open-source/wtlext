// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_COMVARIANT_H__
#define __WTL_COMVARIANT_H__

#include <utility>

#include <atlcomcli.h>
#include <atlsafe.h>

#include "wtlextdef.h"

namespace ATL {

template<>
class CVarTypeInfo<SAFEARRAY**> {
public:
    static const VARTYPE VT = VT_ARRAY | VT_BYREF;
    static SAFEARRAY** VARIANT::*const pmField;
};

__declspec(selectany) SAFEARRAY** VARIANT::*const CVarTypeInfo<SAFEARRAY**>::pmField = &VARIANT::pparray;

} // namespace ATL

namespace WTL {

#if _MSC_VER >= 1600
class CComVariantVeneer : public ATL::CComVariant {
public:
    using CComVariant::CComVariant;

    static CComVariantVeneer MoveConstruct(SAFEARRAY*&& source, bool leaveDanglingPointer = false) ATLVARIANT_THROW();
    CComVariantVeneer& MoveAssign(SAFEARRAY*&& source, bool leaveDanglingPointer = false) ATLVARIANT_THROW();

protected:
    CComVariantVeneer(SAFEARRAY*&& source) ATLVARIANT_THROW();
    CComVariantVeneer& operator=(SAFEARRAY*&& source) ATLVARIANT_THROW();

protected:
    // CComVariant's is private, that's why we have to implement it here again
    void ClearThrow() ATLVARIANT_THROW();
};
#endif

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

// specialisations for CComVariant template methods

template<>
inline void CComVariant::SetByRef<SAFEARRAY*>(SAFEARRAY** source) ATLVARIANT_THROW()
{
    ClearThrow();
    HRESULT hr = AtlSafeArrayGetActualVartype(*source, &vt);
    if (SUCCEEDED(hr)) {
        vt |= CVarTypeInfo<SAFEARRAY**>::VT;
        pparray = source;
    }
    else {
        vt = VT_ERROR;
        scode = hr;
#ifndef _ATL_NO_VARIANT_THROW
        ATLENSURE_THROW(FALSE, hr);
#endif
    }
}

namespace WTL {

#if _MSC_VER >= 1600
inline CComVariantVeneer CComVariantVeneer::MoveConstruct(SAFEARRAY*&& source, bool leaveDanglingPointer) ATLVARIANT_THROW()
{
#if _MSC_VER >= 1900
    return CComVariantVeneer{leaveDanglingPointer ? std::move(source) : std::exchange(source, nullptr)};
#else
    SAFEARRAY* x = source;
    if (!leaveDanglingPointer)
        source = nullptr;
    return CComVariantVeneer(std::move(x));
#endif
}

inline CComVariantVeneer& CComVariantVeneer::MoveAssign(SAFEARRAY*&& source, bool leaveDanglingPointer) ATLVARIANT_THROW()
{
#if _MSC_VER >= 1900
    return *this = leaveDanglingPointer ? std::move(source) : std::exchange(source, nullptr);
#else
    SAFEARRAY* x = source;
    if (!leaveDanglingPointer)
        source = nullptr;
    return *this = std::move(x);
#endif
}

inline CComVariantVeneer::CComVariantVeneer(SAFEARRAY*&& source) ATLVARIANT_THROW()
{
    WTLASSERT(source != nullptr);
    if (!source) {
        vt = VT_ERROR;
        scode = E_INVALIDARG;
#ifndef _ATL_NO_VARIANT_THROW
        WtlThrow(E_INVALIDARG);
#else
        return;
#endif
    }

    HRESULT hr = AtlSafeArrayGetActualVartype(source, &vt);
    if (SUCCEEDED(hr)) {
        vt |= VT_ARRAY;
        parray = source;
    }
    else {
        vt = VT_ERROR;
        scode = hr;
        SafeArrayDestroy(source);
#ifndef _ATL_NO_VARIANT_THROW
        if (FAILED(hr))
            ATLENSURE_THROW(FALSE, hr);
#endif
    }
}

CComVariantVeneer& CComVariantVeneer::operator=(SAFEARRAY*&& source) ATLVARIANT_THROW()
{
    WTLASSERT(source != nullptr);

    if (!source) {
        ClearThrow();
        vt = VT_ERROR;
        scode = E_INVALIDARG;
#ifndef _ATL_NO_VARIANT_THROW
        WtlThrow(E_INVALIDARG);
#endif
    }
    else if (!(vt & VT_ARRAY) || source != parray) {
        ClearThrow();
        HRESULT hr = AtlSafeArrayGetActualVartype(source, &vt);
        if (SUCCEEDED(hr)) {
            vt |= VT_ARRAY;
            parray = source;
            source = nullptr;
        }
        else {
            vt = VT_ERROR;
            scode = hr;
#ifndef _ATL_NO_VARIANT_THROW
            ATLENSURE_THROW(FALSE, hr);
#endif
        }
    }

    return *this;
}

void CComVariantVeneer::ClearThrow() ATLVARIANT_THROW()
{
    HRESULT hr = Clear();
    WTLASSERT(SUCCEEDED(hr));
    (hr);
#ifndef _ATL_NO_VARIANT_THROW
    if (FAILED(hr))
        WtlThrow(hr);
#endif
}
#endif

} // namespace WTL

#endif // __WTL_COMVARIANT_H__
