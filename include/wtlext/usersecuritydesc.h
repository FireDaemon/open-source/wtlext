// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_USEROBJECTSECURITYDESC_H__
#define __WTL_USEROBJECTSECURITYDESC_H__

#include <atlexcept.h>
#include <atlsecurity.h>

#include "wtlextdef.h"

namespace WTL {

class CUserObjectSecurityDesc : public CSecurityDesc {
public:
    CUserObjectSecurityDesc();
    // no transfer of ownership
    explicit CUserObjectSecurityDesc(HANDLE obj);

    bool LoadDescriptor(HANDLE obj);
    void ApplyDescriptor();
    HANDLE UserObject() const
    {
        return obj_;
    }

protected:
    // no ownership
    HANDLE obj_;
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#pragma comment(lib, "AdvAPI32.Lib")

namespace WTL {

inline CUserObjectSecurityDesc::CUserObjectSecurityDesc()
    : obj_(NULL)
{
}

inline CUserObjectSecurityDesc::CUserObjectSecurityDesc(HANDLE obj)
    : obj_(NULL)
{
    if (!LoadDescriptor(obj))
        WtlThrowLastWin32();
}

inline bool CUserObjectSecurityDesc::LoadDescriptor(HANDLE obj)
{
    WTLASSERT(obj);

    obj_ = NULL;
    Clear();
    obj_ = obj;

    SECURITY_INFORMATION si = DACL_SECURITY_INFORMATION;
    DWORD nBytesNeeded;
    if (!GetUserObjectSecurity(obj_, &si, NULL, 0, &nBytesNeeded) && GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
        return false;
    }

    m_pSecurityDescriptor = PISECURITY_DESCRIPTOR(malloc(nBytesNeeded));
    if (!m_pSecurityDescriptor)
        WtlThrow(E_OUTOFMEMORY);

    GetUserObjectSecurity(obj_, &si, m_pSecurityDescriptor, nBytesNeeded, &nBytesNeeded);

    return true;
}

inline void CUserObjectSecurityDesc::ApplyDescriptor()
{
    SECURITY_INFORMATION si = DACL_SECURITY_INFORMATION;
    if (!SetUserObjectSecurity(obj_, &si, m_pSecurityDescriptor))
        WtlThrowLastWin32();
}

} // namespace WTL

#endif // __WTL_USEROBJECTSECURITYDESC_H__
