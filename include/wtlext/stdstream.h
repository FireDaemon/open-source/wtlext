// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_STDSTREAM_H__
#define __WTL_STDSTREAM_H__

#include <ObjIdl.h> // IStream
#include <guiddef.h>
#include <ntverp.h>
#if VER_PRODUCTBUILD >= 9600 // since Windows SDK 8.1
#include <minwindef.h>
#include <winerror.h>
#else
#include <Windows.h>
#endif
#include "wtlextdef.h"

namespace WTL {

/** @short A std stream implementing the ISequentialStream interface.
 *  @note This is not a registered COM object, hence can't be instantiated through the COM mechanism.
 *  Rather use CStdStream::FromStdHandles() or CStdStream::FromHandles().
 */
class CStdStream : public IStream {
private:
    // nullptr_t ctor
    CStdStream(int) throw();

protected:
    CStdStream() throw();
    ~CStdStream();

public:
    static IStream* FromStdHandles();
    static IStream* FromHandles(HANDLE in, HANDLE out, bool takeOwnership);

    // IUnknown Interface
public:
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void** object) _WTL_OVERRIDE;
    ULONG STDMETHODCALLTYPE AddRef() _WTL_OVERRIDE;
    ULONG STDMETHODCALLTYPE Release() _WTL_OVERRIDE;

    // ISequentialStream Interface
public:
    HRESULT STDMETHODCALLTYPE Read(void* data, ULONG nCount, ULONG* nRead) _WTL_OVERRIDE;
    HRESULT STDMETHODCALLTYPE Write(void const* data, ULONG nCount, ULONG* nWritten) _WTL_OVERRIDE;

    // IStream Interface
public:
    HRESULT STDMETHODCALLTYPE SetSize(ULARGE_INTEGER) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE CopyTo(IStream*, ULARGE_INTEGER, ULARGE_INTEGER*, ULARGE_INTEGER*) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Commit(DWORD) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Revert() _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE LockRegion(ULARGE_INTEGER, ULARGE_INTEGER, DWORD) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE UnlockRegion(ULARGE_INTEGER, ULARGE_INTEGER, DWORD) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Clone(IStream**) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Seek(LARGE_INTEGER, DWORD, ULARGE_INTEGER*) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Stat(STATSTG*, DWORD) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }

private:
    // note: alignment as documented on MSDN for InterlockedIncrement
    __declspec(align(32)) LONG ref_;

protected:
    HANDLE in_;
    HANDLE out_;
    bool manage_;
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <Windows.h>

namespace WTL {

inline CStdStream::CStdStream(int) throw()
    : ref_(0)
    , in_(NULL)
    , out_(NULL)
    , manage_(false)
{
}

inline CStdStream::CStdStream() throw()
    : ref_(0)
    , in_(NULL)
    , out_(NULL)
    , manage_(false)
{
}

inline CStdStream::~CStdStream()
{
    if (manage_) {
        if (out_)
            CloseHandle(out_);
        if (in_)
            CloseHandle(in_);
    }
}

inline IStream* CStdStream::FromStdHandles()
{
    return FromHandles(GetStdHandle(STD_INPUT_HANDLE), GetStdHandle(STD_OUTPUT_HANDLE), false);
}

inline IStream* CStdStream::FromHandles(HANDLE in, HANDLE out, bool takeOwnership)
{
    CStdStream* stream = _ATL_NEW CStdStream(NULL);
#ifndef _ATL_DISABLE_NOTHROW_NEW
    if (!stream)
        WtlThrow(E_OUTOFMEMORY);
#endif

    stream->in_ = in;
    stream->out_ = out;
    stream->manage_ = takeOwnership;

    return stream;
}

inline HRESULT STDMETHODCALLTYPE CStdStream::QueryInterface(REFIID iid, void** object)
{
    if (iid == __uuidof(IUnknown) || iid == __uuidof(ISequentialStream) || iid == __uuidof(IStream)) {
        *object = this;
        AddRef();
        return S_OK;
    }
    else
        return E_NOINTERFACE;
}

inline ULONG STDMETHODCALLTYPE CStdStream::AddRef()
{
    return InterlockedIncrement(&ref_);
}

inline ULONG STDMETHODCALLTYPE CStdStream::Release()
{
    LONG n = InterlockedDecrement(&ref_);
    if (n == 0)
        delete this;
    return n;
}

inline HRESULT STDMETHODCALLTYPE CStdStream::Read(void* data, ULONG nCount, ULONG* nRead)
{
    BOOL rc = ReadFile(in_, data, nCount, nRead, NULL);
    return rc ? S_OK : GetLastError() == ERROR_BROKEN_PIPE ? S_FALSE : HRESULT_FROM_WIN32(GetLastError());
}

inline HRESULT STDMETHODCALLTYPE CStdStream::Write(void const* data, ULONG nCount, ULONG* nWritten)
{
    BOOL rc = WriteFile(out_, data, nCount, nWritten, NULL);
    return rc ? S_OK : HRESULT_FROM_WIN32(GetLastError());
}

} // namespace WTL

#endif // __WTL_STDSTREAM_H__
