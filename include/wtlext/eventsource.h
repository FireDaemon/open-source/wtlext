// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_EVENTSOURCE_H__
#define __WTL_EVENTSOURCE_H__

#if _MSC_VER >= 1900
#include <type_traits>
#endif
#if _MSC_VER >= 1800
#include <initializer_list>
#endif
#include <intsafe.h>
#include <ntverp.h>
#include <tchar.h>
#if VER_PRODUCTBUILD >= 9600 // since Windows SDK 8.1
#include <minwindef.h>
#else
#include <Windows.h>
#endif

namespace WTL {

class CEventSource {
public:
    // event code consists of:
    // - event id (low word)
    // - qualifier (high word)
    //   - severity (bits 15+16)
    static const int SeverityValueToWordBitShift = 14;

    enum SeverityWord : WORD {
        SeveritySuccess,
        SeverityInfo = 1 << SeverityValueToWordBitShift,
        SeverityWarning,
        SeverityError,
    };

    struct EvtCat {
        EvtCat(DWORD eventCode)
            : eventCode(eventCode)
            , categoryId(0)
        {
        }
        EvtCat(DWORD eventCode, DWORD categoryCode)
            : eventCode(eventCode)
            , categoryId(LOWORD(categoryCode))
        {
        }
        EvtCat(DWORD64 evtAndCatCodes)
            : eventCode(LODWORD(evtAndCatCodes))
            , categoryId(LOWORD(HIDWORD(evtAndCatCodes)))
        {
        }
        EvtCat(LONG eventCode)
            : eventCode(eventCode)
            , categoryId(0)
        {
        }
        EvtCat(LONG eventCode, LONG categoryCode)
            : eventCode(eventCode)
            , categoryId(LOWORD(categoryCode))
        {
        }
        EvtCat(LONG64 evtAndCatCodes)
            : eventCode(LODWORD(evtAndCatCodes))
            , categoryId(LOWORD(HIDWORD(evtAndCatCodes)))
        {
        }

        DWORD eventCode;
        WORD categoryId;
    };

public:
    CEventSource();
    explicit CEventSource(const wchar_t* sourceName, const wchar_t* serverNameInUNC = NULL);
    ~CEventSource();

    bool Register(const wchar_t* sourceName, const wchar_t* serverNameInUNC = NULL);
    void Deregister();

    operator HANDLE() const
    {
        return eventSrc_;
    }

    void Report(EvtCat eventAndCategory, const _TCHAR* const msgParams[], size_t nParams) const
    {
        return ReportAs(NULL, eventAndCategory, msgParams, nParams);
    }

    template<size_t N>
    void Report(EvtCat eventAndCategory, const _TCHAR* const (&msgParams)[N]) const
    {
        return ReportAs(NULL, eventAndCategory, msgParams, N);
    }

#if _MSC_VER >= 1800
    void Report(EvtCat eventAndCategory, std::initializer_list<const _TCHAR*> msgParams) const
    {
        return ReportAs(nullptr, eventAndCategory, msgParams.begin(), msgParams.size());
    }
#endif

#if _MSC_VER >= 1900
    template<typename... CStrLike,
             std::enable_if_t<std::conjunction_v<std::is_convertible<std::remove_reference_t<CStrLike>, const _TCHAR*>...>, int> = 0>
    void Report(EvtCat eventAndCategory, CStrLike&&... msgParams) const
    {
        constexpr size_t nParams = sizeof...(CStrLike);
        const _TCHAR* data[nParams ? nParams : 1] = {msgParams...};
        return ReportAs(nullptr, eventAndCategory, data, nParams);
    }
#endif

    void ReportAs(const SID* user, EvtCat eventAndCategory, const _TCHAR* const msgParams[], size_t nParams) const;

    template<size_t N>
    void ReportAs(const SID* user, EvtCat eventAndCategory, const _TCHAR* const (&msgParams)[N]) const
    {
        return ReportAs(user, eventAndCategory, msgParams, N);
    }
#if _MSC_VER >= 1800
    void Report(const SID* user, EvtCat eventAndCategory, std::initializer_list<const _TCHAR*> msgParams) const
    {
        return ReportAs(user, eventAndCategory, msgParams.begin(), msgParams.size());
    }
#endif

#if _MSC_VER >= 1900
    template<typename... CStrLike,
             std::enable_if_t<std::conjunction_v<std::is_convertible<std::remove_reference_t<CStrLike>, const _TCHAR*>...>, int> = 0>
    void ReportAs(const SID* user, EvtCat eventAndCategory, CStrLike&&... msgParams) const
    {
        constexpr size_t nParams = sizeof...(CStrLike);
        const _TCHAR* data[nParams ? nParams : 1] = {msgParams...};
        return ReportAs(user, eventAndCategory, data, nParams);
    }
#endif

private:
    HANDLE eventSrc_;
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <Windows.h>
#include <atlenc.h>
#include <atlstr.h>
#include <intsafe.h>
#include <string.h>

#include "wtlextdef.h"

namespace WTL {

inline CEventSource::CEventSource()
    : eventSrc_(NULL)
{
}

inline CEventSource::CEventSource(const wchar_t* sourceName, const wchar_t* serverNameInUNC)
    : eventSrc_(NULL)
{
    if (!Register(sourceName, serverNameInUNC))
        WtlThrowLastWin32();
}

inline CEventSource::~CEventSource()
{
    if (eventSrc_)
        DeregisterEventSource(eventSrc_);
}

inline bool CEventSource::Register(const wchar_t* sourceName, const wchar_t* serverNameInUNC)
{
    WTLASSERT(sourceName && *sourceName);

    Deregister();

    ATL::CAtlStringW escaped;
    const size_t nLen = wcslen(sourceName);
    int needed = ATL::EscapeXML(sourceName, int(nLen), NULL, 0, ATL_ESC_FLAG_ATTR);

    const wchar_t* xmlattr;
    if (needed == nLen)
        xmlattr = sourceName;
    else {
        needed = ATL::EscapeXML(sourceName, int(nLen), escaped.GetBufferSetLength(needed), needed, ATL_ESC_FLAG_ATTR);
        WTLASSERT(needed);
        xmlattr = escaped;
    }

    eventSrc_ = RegisterEventSourceW(serverNameInUNC, xmlattr);
    return eventSrc_ || false;
}

inline void CEventSource::ReportAs(const SID* user, EvtCat eventAndCategory, const _TCHAR* const msgParams[], size_t nParams) const
{
    WTLASSERT(eventSrc_);

    const WORD severityToType[4] = {EVENTLOG_SUCCESS, EVENTLOG_INFORMATION_TYPE, EVENTLOG_WARNING_TYPE, EVENTLOG_ERROR_TYPE};
    // extract severity value from event id
    const WORD severityValue = HIWORD(eventAndCategory.eventCode) >> SeverityValueToWordBitShift;
    if (!ReportEvent(eventSrc_, severityToType[severityValue], eventAndCategory.categoryId, eventAndCategory.eventCode, PISID(user),
                     WORD(nParams), 0, PZPCWSTR(msgParams), NULL))
        WtlThrowLastWin32();
}

inline void CEventSource::Deregister()
{
    if (eventSrc_) {
        DeregisterEventSource(eventSrc_);
        eventSrc_ = NULL;
    }
}

} // namespace WTL

#endif // __WTL_EVENTSOURCE_H__
