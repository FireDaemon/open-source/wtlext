// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_SECURITYPOLICY_H__
#define __WTL_SECURITYPOLICY_H__

#if _MSC_VER >= 1800
#include <array>
#endif
#include <ntverp.h>
#include <stddef.h>
#include <tchar.h>
#if VER_PRODUCTBUILD >= 9600 // since Windows SDK 8.1
#include <minwindef.h>
#else
#include <Windows.h>
#endif
#include <NTSecAPI.h>

namespace WTL {

class CSecurityPolicy {
public:
    CSecurityPolicy() throw();
    explicit CSecurityPolicy(const _TCHAR* szAccountName, DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT);
    explicit CSecurityPolicy(const SID* account, DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT);
    CSecurityPolicy(const wchar_t* szServerName, const _TCHAR* szAccountName,
                    DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT);
    CSecurityPolicy(const wchar_t* szServerName, const SID* account, DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT);
    ~CSecurityPolicy();

    DWORD Open(const _TCHAR* szAccountName, DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT) throw();
    DWORD Open(const SID* account, DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT) throw();
    DWORD Open(const wchar_t* szServerName, const _TCHAR* szAccountName,
               DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT) throw();
    DWORD Open(const wchar_t* szServerName, const SID* account, DWORD dwAccess = POLICY_LOOKUP_NAMES | POLICY_CREATE_ACCOUNT) throw();
    void Close() throw();

    DWORD EnablePrivilege(const wchar_t* szPrivilege) throw()
    {
        return EnablePrivileges<1>(&szPrivilege);
    }
    DWORD RemovePrivilege(const wchar_t* szPrivilege) throw()
    {
        return RemovePrivileges<1>(&szPrivilege);
    }

    template<size_t N>
    DWORD EnablePrivileges(const wchar_t* const (&privileges)[N]) throw()
    {
        const wchar_t* const* decayed = privileges;
        return EnablePrivileges<N>(decayed);
    }
#if _MSC_VER >= 1800
    template<size_t N>
    DWORD EnablePrivileges(const std::array<const wchar_t*, N>& privileges) throw()
    {
        return EnablePrivileges<N>(privileges.data());
    }
#endif

    template<size_t N>
    DWORD RemovePrivileges(const wchar_t* const (&privileges)[N]) throw()
    {
        const wchar_t* const* decayed = privileges;
        return RemovePrivileges<N>(decayed);
    }
#if _MSC_VER >= 1800
    template<size_t N>
    DWORD RemovePrivileges(const std::array<const wchar_t*, N>& privileges) throw()
    {
        return RemovePrivileges<N>(privileges.data());
    }
#endif

private:
    template<size_t N>
    DWORD EnablePrivileges(const wchar_t* const privileges[]) throw();
    template<size_t N>
    DWORD RemovePrivileges(const wchar_t* const privileges[]) throw();

private:
    SID* accountSid_;
    LSA_HANDLE hPolicy_;
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <new>

#include <Windows.h>
#include <atldef.h>
#include <atlsecurity.h>
#include <string.h>

#include "wtlextdef.h"

#pragma comment(lib, "AdvAPI32.Lib")

namespace WTL {

struct LsaUnicodeString : LSA_UNICODE_STRING {
    operator PLSA_UNICODE_STRING() throw()
    {
        return this;
    }
};

static inline LsaUnicodeString AtlConstCastToLsaString(const wchar_t* String) throw()
{
    USHORT len = USHORT(String ? wcslen(String) : 0);
    LsaUnicodeString lsa = {{USHORT(len * sizeof(wchar_t)), USHORT((len + 1) * sizeof(wchar_t)), PWSTR(String)}};
    return lsa;
}

inline CSecurityPolicy::CSecurityPolicy() throw()
    : accountSid_(NULL)
    , hPolicy_(NULL)
{
}

inline CSecurityPolicy::~CSecurityPolicy()
{
    if (hPolicy_)
        LsaClose(hPolicy_);
    operator delete[](accountSid_, std::nothrow);
}

inline CSecurityPolicy::CSecurityPolicy(const _TCHAR* szAccountName, DWORD dwAccess)
    : accountSid_(NULL)
    , hPolicy_(NULL)
{
    if (DWORD r = Open(szAccountName, dwAccess))
        WtlThrow(HRESULT_FROM_WIN32(r));
}

inline CSecurityPolicy::CSecurityPolicy(const SID* account, DWORD dwAccess)
    : accountSid_(NULL)
    , hPolicy_(NULL)
{
    if (DWORD r = Open(account, dwAccess))
        WtlThrow(HRESULT_FROM_WIN32(r));
}

inline CSecurityPolicy::CSecurityPolicy(const wchar_t* szServerName, const _TCHAR* szAccountName, DWORD dwAccess)
    : accountSid_(NULL)
    , hPolicy_(NULL)
{
    if (DWORD r = Open(szServerName, szAccountName, dwAccess))
        WtlThrow(HRESULT_FROM_WIN32(r));
}

inline CSecurityPolicy::CSecurityPolicy(const wchar_t* szServerName, const SID* account, DWORD dwAccess)
    : accountSid_(NULL)
    , hPolicy_(NULL)
{
    if (DWORD r = Open(szServerName, account, dwAccess))
        WtlThrow(HRESULT_FROM_WIN32(r));
}

inline DWORD CSecurityPolicy::Open(const _TCHAR* szAccountName, DWORD dwAccess) throw()
{
    return Open(L"", szAccountName, dwAccess);
}

inline DWORD CSecurityPolicy::Open(const SID* account, DWORD dwAccess) throw()
{
    return Open(L"", account, dwAccess);
}

inline DWORD CSecurityPolicy::Open(const wchar_t* szServerName, const _TCHAR* szAccountName, DWORD dwAccess) throw()
{
    if (!szServerName || !szAccountName || !*szAccountName)
        return ERROR_INVALID_PARAMETER;

    if (!_tcsncmp(szAccountName, _T(".\\"), 2))
        szAccountName += 2;

    CSid account;
    if (!account.LoadAccount(szAccountName))
        return GetLastError();

    return Open(szServerName, account, dwAccess);
}

inline DWORD CSecurityPolicy::Open(const wchar_t* szServerName, const SID* account, DWORD dwAccess) throw()
{
    if (!szServerName || !account)
        return ERROR_INVALID_PARAMETER;

    Close();

    DWORD len = GetLengthSid(PISID(account));
    accountSid_ = PISID(operator new[](len, std::nothrow));
    if (!accountSid_)
        return ERROR_NOT_ENOUGH_MEMORY;
    CopySid(len, accountSid_, PISID(account));

    LSA_OBJECT_ATTRIBUTES attrs = {};
    if (NTSTATUS r = LsaOpenPolicy(AtlConstCastToLsaString(szServerName), &attrs, dwAccess, &hPolicy_))
        return LsaNtStatusToWinError(r);

    return ERROR_SUCCESS;
}

inline void CSecurityPolicy::Close() throw()
{
    if (hPolicy_)
        LsaClose(hPolicy_);
    hPolicy_ = NULL;

    operator delete[](accountSid_, std::nothrow);
    accountSid_ = NULL;
}

template<size_t N>
inline DWORD CSecurityPolicy::EnablePrivileges(const wchar_t* const privileges[]) throw()
{
    WTLASSERT(hPolicy_ && accountSid_);

    LSA_UNICODE_STRING us[N];
    for (size_t i = 0; i < N; ++i)
        us[i] = AtlConstCastToLsaString(privileges[i]);

    if (NTSTATUS r = LsaAddAccountRights(hPolicy_, accountSid_, us, N))
        return LsaNtStatusToWinError(r);
    return ERROR_SUCCESS;
}

template<size_t N>
inline DWORD CSecurityPolicy::RemovePrivileges(const wchar_t* const privileges[]) throw()
{
    WTLASSERT(hPolicy_ && accountSid_);

    LSA_UNICODE_STRING us[N];
    for (size_t i = 0; i < N; ++i)
        us[i] = AtlConstCastToLsaString(privileges[i]);

    if (NTSTATUS r = LsaRemoveAccountRights(hPolicy_, accountSid_, false, us, N))
        return LsaNtStatusToWinError(r);
    return ERROR_SUCCESS;
}

} // namespace WTL

#endif // __WTL_SECURITYPOLICY_H__
