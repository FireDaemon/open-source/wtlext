// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_ALLOC_H__
#define __WTL_ALLOC_H__

#include <new>

#include <atldef.h>

#include "wtlextdef.h"

namespace WTL {

// Type declarations
class CCppAllocator {
public:
    _Ret_maybenull_ _ATL_DECLSPEC_ALLOCATOR static void* Reallocate(_In_ void* p, _In_ size_t nBytes) throw()
    {
        ::operator delete(p, std::nothrow);
        return ::operator new(nBytes, std::nothrow);
    }

    _Ret_maybenull_ _ATL_DECLSPEC_ALLOCATOR static void* Allocate(_In_ size_t nBytes) throw()
    {
        return ::operator new(nBytes, std::nothrow);
    }

    static void Free(_In_ void* p) throw()
    {
        ::operator delete(p, std::nothrow);
    }
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#endif // __WTL_ALLOC_H__
