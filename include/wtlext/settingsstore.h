// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_SETTINGSSTORE_H__
#define __WTL_SETTINGSSTORE_H__

#include <atlbase.h>
#include <atlsimpstr.h>

#include "alloc.h"
#include "regkeyex.h"
#include "wtlextdef.h"

namespace WTL {

class CRegSetting {
    template<typename TAggregate>
    static TAggregate init_aggregate_0()
    {
        TAggregate tmp = {};
        return tmp;
    }
    template<typename TAggregate, typename Arg1, typename Arg2>
    static TAggregate init_aggregate_2(Arg1 arg1, Arg2 arg2)
    {
        TAggregate tmp = {arg1, arg2};
        return tmp;
    }

    // Type declarations
public:
    template<typename ATLAllocator = CCppAllocator>
    struct BinarySetting;

    template<>
    struct BinarySetting<void> {
        typedef void Allocator;

        struct Data {
            void* pBinary;
            ULONG uSize; // size in bytes
        } data, *palias; // aliasing for non-const access

        BinarySetting(void* pBinary, ULONG uSize) throw()
            : data(init_aggregate_2<Data>(pBinary, uSize))
            , palias(&data)
        {
        }
        template<size_t Size>
        BinarySetting(BYTE (&pBinary)[Size]) throw()
            : data(init_aggregate_2<Data>(pBinary, Size))
            , palias(&data)
        {
        }

    protected:
        BinarySetting() throw()
            : data(init_aggregate_0<Data>())
            , palias(&data)
        {
        }
    };

    template<typename ATLAllocator>
    struct BinarySetting : BinarySetting<void> {
        typedef ATLAllocator Allocator;

        BinarySetting() throw() {}
        BinarySetting(void* pBinary, ULONG uSize) throw()
            : BinarySetting<void>(pBinary, uSize)
        {
        }

#if _MSC_VER >= 1600
        BinarySetting& operator=(BinarySetting::Data&& other) throw()
#else
        BinarySetting& operator=(BinarySetting::Data& other) throw()
#endif
        {
            if (&this->data != &other) {
                this->data = other;
                other = init_aggregate_0<BinarySetting::Data>();
            }
            return *this;
        }

        ~BinarySetting() throw()
        {
            Allocator::Free(this->data.pBinary);
        }

    private:
        BinarySetting(const BinarySetting&);
        BinarySetting& operator=(const BinarySetting&);
    };

    template<typename ATLAllocator = CCppAllocator>
    struct CharArraySetting;

    template<>
    struct CharArraySetting<void> {
        typedef void Allocator;

        struct Data {
            PTSTR szText;
            ULONG uSize; // size in characters, including 0-terminator
        } data, *palias; // aliasing for non-const access

        CharArraySetting(PTSTR szText, ULONG uSize) throw()
            : data(init_aggregate_2<Data>(szText, uSize))
            , palias(&data)
        {
        }
        template<size_t Size>
        CharArraySetting(_TCHAR (&szText)[Size]) throw()
            : data(init_aggregate_2<Data>(szText, Size))
            , palias(&data)
        {
        }

    protected:
        CharArraySetting() throw()
            : data(init_aggregate_0<Data>())
            , palias(&data)
        {
        }
    };

    template<typename ATLAllocator>
    struct CharArraySetting : CharArraySetting<void> {
        typedef ATLAllocator Allocator;

        CharArraySetting() throw() {}
        CharArraySetting(PTSTR szText, ULONG uSize) throw()
            : CharArraySetting<void>(szText, uSize)
        {
        }

#if _MSC_VER >= 1600
        CharArraySetting& operator=(CharArraySetting::Data&& other) throw()
#else
        CharArraySetting& operator=(CharArraySetting::Data& other) throw()
#endif
        {
            if (&this->data != &other) {
                this->data = other;
                other = init_aggregate_0<CharArraySetting::Data>();
            }
            return *this;
        }

        ~CharArraySetting() throw()
        {
            Allocator::Free(this->data.szText);
        }

    private:
        CharArraySetting(const CharArraySetting&);
        CharArraySetting& operator=(const CharArraySetting&);
    };

    // Type erasure helper to facilitate handling DWORD/QWORD values
    template<typename ValueType>
    struct TSettingVTable {
        ValueType (*vget)(void*);
        void (*vset)(void*, ValueType);

        template<typename TSetting>
        static const TSettingVTable* ForTSetting()
        {
            static TSettingVTable x = {&TSettingVTable::GetValue<TSetting>, &TSettingVTable::SetValue<TSetting>};
            return &x;
        }

    private:
        template<typename TSetting>
        static ValueType GetValue(void* propAddress)
        {
            return ValueType(*(TSetting*)propAddress);
        }
        template<typename TSetting>
        static void SetValue(void* propAddress, ValueType v)
        {
            *(TSetting*)propAddress = TSetting(v);
        }
    };

#define TWORD_SETTING_CTOR(T, TSetting)                              \
    T##WordSetting(TSetting& prop) throw()                           \
        : propAddress(&prop)                                         \
        , vtable(TSettingVTable<ValueType>::ForTSetting<TSetting>()) \
    {                                                                \
    }

    // Convert anything that looks like a 32-bit (or less) built-in value
    struct DWordSetting {
        typedef DWORD ValueType;

        TWORD_SETTING_CTOR(D, unsigned long)
        TWORD_SETTING_CTOR(D, long)
        TWORD_SETTING_CTOR(D, unsigned int)
        TWORD_SETTING_CTOR(D, int)
        TWORD_SETTING_CTOR(D, unsigned short)
        TWORD_SETTING_CTOR(D, short)
        TWORD_SETTING_CTOR(D, unsigned char)
        TWORD_SETTING_CTOR(D, signed char)
        TWORD_SETTING_CTOR(D, char)
        TWORD_SETTING_CTOR(D, bool)
#ifdef _NATIVE_WCHAR_T_DEFINED
        TWORD_SETTING_CTOR(D, wchar_t)
#endif

        operator ValueType() const
        {
            return vtable->vget(propAddress);
        }
        void operator=(ValueType v) const
        {
            return vtable->vset(propAddress, v);
        }

    private:
        void* propAddress;
        const TSettingVTable<ValueType>* vtable;
    };
    // Convert anything that looks like a 64-bit built-in value
    struct QWordSetting {
        typedef ULONGLONG ValueType;

        TWORD_SETTING_CTOR(Q, unsigned long long)
        TWORD_SETTING_CTOR(Q, long long)

        operator ValueType() const
        {
            return vtable->vget(propAddress);
        }
        void operator=(ValueType v) const
        {
            return vtable->vset(propAddress, v);
        }

    private:
        void* propAddress;
        const TSettingVTable<ValueType>* vtable;
    };

#undef TWORD_SETTING_CTOR

    template<typename TSetting>
    static DWordSetting AsDWordSetting(TSetting& prop)
    {
        return prop;
    }
    template<typename TSetting>
    static QWordSetting AsQWordSetting(TSetting& prop)
    {
        return prop;
    }

    // Data members
    CRegKeyEx2 m_regkey;

    // Construction/Destruction
    explicit CRegSetting(bool bGlobal, bool bWithSubKeyPaths = false) throw();

#if _MSC_VER >= 1600
    CRegSetting(CRegSetting&& other) throw()
        : m_regkey{std::move(other.m_regkey)}
    {
    }
#endif

    // Storage interface

    static LONG Delete(bool bGlobal, PCTSTR szRegSubKey) throw();
    LONG Open(PCTSTR szRegSubKey, bool bReadWrite) throw();
    bool IsOpen() const throw();
    void Close() throw();

    void DeleteValue(PCTSTR szRegValue) throw();

    // Generic read/write methods (DWORD)
    LONG ReadValue(PCTSTR szRegValue, const DWordSetting& prop) throw();
    LONG WriteValue(PCTSTR szRegValue, const DWordSetting& prop) throw();

    // Generic read/write methods (QWORD)
    LONG ReadValue(PCTSTR szRegValue, const QWordSetting& prop) throw();
    LONG WriteValue(PCTSTR szRegValue, const QWordSetting& prop) throw();

    // Specialization for HFONT
    LONG ReadValue(PCTSTR szRegValue, HFONT& hFont) throw();
    LONG WriteValue(PCTSTR szRegValue, HFONT hFont) throw();

    // Specialization for BinarySetting
    LONG ReadValue(PCTSTR szRegValue, const BinarySetting<void>& binSetting) throw();
    template<typename ATLAllocator>
    LONG ReadValue(PCTSTR szRegValue, const BinarySetting<ATLAllocator>& binSetting) throw();
    LONG WriteValue(PCTSTR szRegValue, const BinarySetting<void>& binSetting) throw();

    // Specialization for CharArraySetting
    LONG ReadValue(PCTSTR szRegValue, const CharArraySetting<void>& caSetting) throw();
    template<typename ATLAllocator>
    LONG ReadValue(PCTSTR szRegValue, const CharArraySetting<ATLAllocator>& caSetting) throw();
    LONG WriteValue(PCTSTR szRegValue, const CharArraySetting<void>& caSetting) throw();

// Specialization for CString
#ifdef __ATLSIMPSTR_H__
    LONG ReadValue(PCTSTR szRegValue, ATL::CSimpleString& strSetting);
    LONG WriteValue(PCTSTR szRegValue, const ATL::CSimpleString& strSetting) throw();
#endif // __ATLSIMPSTR_H__

    // Static methods for one time read/write
    template<typename TSetting>
    static bool ReadOne(bool bGlobal, PCTSTR szRegSubKey, PCTSTR szRegValue, TSetting& prop);
    template<typename TSetting>
    static bool WriteOne(bool bGlobal, PCTSTR szRegSubKey, PCTSTR szRegValue, TSetting& prop);
};

#define WTL_DECLARE_SETTINGSSTORE_MAP() void DoDataExchange(bool bWrite);

#define WTL_DEFINE_SETTINGSSTORE_MAP(theClass) \
    void theClass::DoDataExchange(bool bWrite) \
    {

#define WTL_BEGIN_SETTINGSSTORE_MAP(theClass) \
    void DoDataExchange(bool bWrite)          \
    {

#define WTL_SETTINGSSTORE_ENTRY(name, setting) this->DoSettingDataExchange(name, setting, bWrite);

#define WTL_END_SETTINGSSTORE_MAP() }

template<typename T, typename TBackend = CRegSetting>
class CSettingsStoreImpl : protected TBackend {
    // Construction/Destruction
protected:
    CSettingsStoreImpl(bool bGlobal, bool bWithSubKeyPaths = false)
        : TBackend(bGlobal, bWithSubKeyPaths)
    {
    }

    CSettingsStoreImpl(bool bGlobal, bool bReadWrite, PCTSTR szPath, bool bWithSubKeyPaths = false)
        : TBackend(bGlobal, bWithSubKeyPaths)
    {
        LONG lRet = this->OpenStore(szPath, bReadWrite);
        if (lRet == ERROR_SUCCESS) {
        }
        else if (lRet != ERROR_FILE_NOT_FOUND || bReadWrite) {
            WtlThrow(HRESULT_FROM_WIN32(lRet));
        }
    }

    // Methods
public:
    static LONG DeleteStore(bool bGlobal, PCTSTR szPath)
    {
        return TBackend::Delete(bGlobal, szPath);
    }

    bool OpenStore(PCTSTR szPath, bool bReadWrite)
    {
        LONG lRet = TBackend::Open(szPath, bReadWrite);
        if ((lRet == ERROR_SUCCESS) || (lRet == ERROR_FILE_NOT_FOUND)) {
            return true;
        }
        else {
            T* pT = static_cast<T*>(this);
            if (bReadWrite)
                pT->OnSettingWriteError(NULL, lRet);
            else
                pT->OnSettingReadError(NULL, lRet);

            return false;
        }
    }

    bool IsStoreOpen() const
    {
        return TBackend::IsOpen();
    }

    void CloseStore()
    {
        TBackend::Close();
    }

    // Overridable methods
    void ReadAllSettings()
    {
        if (!TBackend::IsOpen())
            return;

        T* pT = static_cast<T*>(this);
        pT->DoDataExchange(false);
        pT->OnSettingsRead();
    }

    void WriteAllSettings()
    {
        T* pT = static_cast<T*>(this);
        pT->DoDataExchange(true);
        pT->OnSettingsWritten();
    }

    // Static methods for one time read/write
    static void ReadSettingsAndClose(bool bGlobal, PCTSTR szPath)
    {
        T ps(bGlobal);

        T* pT = &ps;
        LONG lRet = pT->CSettingsStoreImpl::OpenStore(szPath, false);
        if (lRet == ERROR_SUCCESS) {
            pT->ReadAllSettings();

            pT->CSettingsStoreImpl::CloseStore();
        }
    }

    static void WriteSettingsAndClose(bool bGlobal, PCTSTR szPath)
    {
        T ps(bGlobal);

        T* pT = &ps;
        LONG lRet = pT->CSettingsStoreImpl::OpenStore(szPath, true);
        if (lRet == ERROR_SUCCESS) {
            pT->WriteAllSettings();

            pT->CSettingsStoreImpl::CloseStore();
        }
    }

    // Implementation
    template<typename TSetting>
    void DoSettingDataExchange(PCTSTR szValueName, TSetting& prop, bool bWrite)
    {
        if (bWrite)
            DoWriteSetting(szValueName, prop);
        else
            DoReadSetting(szValueName, prop);
    }
    template<typename TSetting>
    void DoReadSetting(PCTSTR szValueName, TSetting& prop)
    {
        T* pT = static_cast<T*>(this);
        LONG lRet = TBackend::ReadValue(szValueName, prop);
        if ((lRet != ERROR_SUCCESS) && (lRet != ERROR_FILE_NOT_FOUND))
            pT->OnSettingReadError(szValueName, lRet);
    }
    template<typename TSetting>
    void DoWriteSetting(PCTSTR szValueName, TSetting& prop)
    {
        T* pT = static_cast<T*>(this);
        LONG lRet = TBackend::WriteValue(szValueName, prop);
        if (lRet != ERROR_SUCCESS)
            pT->OnSettingWriteError(szValueName, lRet);
    }

    // Overrideable handlers
    void OnSettingsRead() {}

    void OnSettingsWritten() {}

    void OnSettingReadError(PCTSTR /*szValueName*/, LONG /*lError*/)
    {
        WTLASSERT(false);
    }

    void OnSettingWriteError(PCTSTR /*szValueName*/, LONG /*lError*/)
    {
        WTLASSERT(false);
    }
};

}

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <wingdi.h>

namespace WTL {

inline CRegSetting::CRegSetting(bool bGlobal, bool bWithSubKeyPaths) throw()
    : m_regkey(bGlobal ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER, bWithSubKeyPaths)
{
}

inline LONG CRegSetting::Delete(bool bGlobal, PCTSTR szRegSubKey) throw()
{
    CRegSetting rs(bGlobal);
    return rs.m_regkey.RecurseDeleteKey(szRegSubKey);
}

inline LONG CRegSetting::Open(PCTSTR szRegSubKey, bool bReadWrite) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    LSTATUS lRet;
    if (bReadWrite)
        lRet = m_regkey.Create(m_regkey.m_hKey, szRegSubKey);
    else
        lRet = m_regkey.Open(m_regkey.m_hKey, szRegSubKey, KEY_READ);

    return lRet;
}

inline bool CRegSetting::IsOpen() const throw()
{
    return m_regkey.m_hKey && (m_regkey.m_hKey != HKEY_LOCAL_MACHINE) && (m_regkey.m_hKey != HKEY_CURRENT_USER);
}

inline void CRegSetting::Close() throw()
{
    LSTATUS lRet = m_regkey.Close();
    (void)lRet; // avoid level 4 warning
    WTLASSERT(lRet == ERROR_SUCCESS);
}

inline void CRegSetting::DeleteValue(PCTSTR szRegValue) throw()
{
    LSTATUS lRet = m_regkey.DeleteValue(szRegValue);
    (void)lRet; // avoid level 4 warning
    WTLASSERT(lRet == ERROR_SUCCESS || lRet == ERROR_FILE_NOT_FOUND);
}

inline LONG CRegSetting::ReadValue(PCTSTR szRegValue, const DWordSetting& prop) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    DWORD dwRet = 0;
    LSTATUS lRet = m_regkey.QueryDWORDValue(szRegValue, dwRet);
    if (lRet == ERROR_SUCCESS)
        prop = dwRet;

    return lRet;
}

inline LONG CRegSetting::WriteValue(PCTSTR szRegValue, const DWordSetting& prop) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    return m_regkey.SetDWORDValue(szRegValue, prop);
}

inline LONG CRegSetting::ReadValue(PCTSTR szRegValue, const QWordSetting& prop) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    ULONGLONG dwRet = 0;
    LSTATUS lRet = m_regkey.QueryQWORDValue(szRegValue, dwRet);
    if (lRet == ERROR_SUCCESS)
        prop = dwRet;

    return lRet;
}

inline LONG CRegSetting::WriteValue(PCTSTR szRegValue, const QWordSetting& prop) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    return m_regkey.SetQWORDValue(szRegValue, prop);
}

inline LONG CRegSetting::ReadValue(PCTSTR szRegValue, HFONT& hFont) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    LOGFONT lf = {};
    ULONG uSize = sizeof(lf);
    LSTATUS lRet = m_regkey.QueryBinaryValue(szRegValue, &lf, &uSize);
    if (lRet == ERROR_SUCCESS) {
        if (hFont != NULL)
            ::DeleteObject(hFont);

        hFont = ::CreateFontIndirect(&lf);
        if (hFont == NULL)
            lRet = ERROR_INVALID_DATA;
    }

    return lRet;
}

inline LONG CRegSetting::WriteValue(PCTSTR szRegValue, HFONT hFont) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    LOGFONT lf = {};
    WTLASSERT(::GetObjectType(hFont) == OBJ_FONT);
    ::GetObject(hFont, sizeof(LOGFONT), &lf);
    return m_regkey.SetBinaryValue(szRegValue, &lf, sizeof(lf));
}

inline LONG CRegSetting::ReadValue(PCTSTR szRegValue, const BinarySetting<void>& binSetting) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    return m_regkey.QueryBinaryValue(szRegValue, binSetting.palias->pBinary, &binSetting.palias->uSize);
}

template<typename ATLAllocator>
LONG CRegSetting::ReadValue(PCTSTR szRegValue, const BinarySetting<ATLAllocator>& binSetting) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    ULONG uBytes = 0u;
    LSTATUS lRet = m_regkey.QueryBinaryValue(szRegValue, NULL, &uBytes);
    if (lRet == ERROR_SUCCESS) {
        if (uBytes <= binSetting.data.uSize) {
            lRet = m_regkey.QueryBinaryValue(szRegValue, binSetting.palias->pBinary, &(binSetting.palias->uSize = uBytes));
        }
        else if (!binSetting.data.uSize && (binSetting.palias->pBinary = ATLAllocator::Allocate(uBytes))) {
            lRet = m_regkey.QueryBinaryValue(szRegValue, binSetting.palias->pBinary, &(binSetting.palias->uSize = uBytes));
        }
        else {
            lRet = ERROR_OUTOFMEMORY;
        }
    }

    return lRet;
}

inline LONG CRegSetting::WriteValue(PCTSTR szRegValue, const BinarySetting<void>& binSetting) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    return m_regkey.SetBinaryValue(szRegValue, binSetting.data.pBinary, binSetting.data.uSize);
}

inline LONG CRegSetting::ReadValue(PCTSTR szRegValue, const CharArraySetting<void>& caSetting) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    return m_regkey.QueryStringValue(szRegValue, caSetting.palias->szText, &caSetting.palias->uSize);
}

template<typename ATLAllocator>
LONG CRegSetting::ReadValue(PCTSTR szRegValue, const CharArraySetting<ATLAllocator>& caSetting) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    ULONG uSize = 0u;
    LSTATUS lRet = m_regkey.QueryStringValue(szRegValue, NULL, &uSize);
    if (lRet == ERROR_SUCCESS) {
        if (uSize <= caSetting.data.uSize) {
            lRet = m_regkey.QueryStringValue(szRegValue, caSetting.palias->szText, &(caSetting.palias->uSize = uSize));
        }
        else if (!caSetting.data.uSize && (caSetting.palias->szText = (PTSTR)ATLAllocator::Allocate(uSize * sizeof(_TCHAR)))) {
            lRet = m_regkey.QueryStringValue(szRegValue, caSetting.palias->szText, &(caSetting.palias->uSize = uSize));
        }
        else {
            lRet = ERROR_OUTOFMEMORY;
        }
    }

    return lRet;
}

inline LONG CRegSetting::WriteValue(PCTSTR szRegValue, const CharArraySetting<void>& caSetting) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    return m_regkey.SetStringValue(szRegValue, caSetting.data.szText, REG_SZ, caSetting.data.uSize);
}

#ifdef __ATLSIMPSTR_H__
inline LONG CRegSetting::ReadValue(PCTSTR szRegValue, ATL::CSimpleString& strSetting)
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    ULONG uSize = 0u;
    LSTATUS lRet = m_regkey.QueryStringValue(szRegValue, NULL, &uSize);
    if (lRet == ERROR_SUCCESS) {
        lRet = m_regkey.QueryStringValue(szRegValue, strSetting.GetBufferSetLength(uSize), &uSize);
        strSetting.ReleaseBuffer();
    }

    return lRet;
}

inline LONG CRegSetting::WriteValue(PCTSTR szRegValue, const ATL::CSimpleString& strSetting) throw()
{
    WTLASSERT(m_regkey.m_hKey != NULL);

    return m_regkey.SetStringValue(szRegValue, strSetting);
}
#endif // __ATLSIMPSTR_H__

template<typename TSetting>
bool CRegSetting::ReadOne(bool bGlobal, PCTSTR szRegSubKey, PCTSTR szRegValue, TSetting& prop)
{
    CRegSetting rs(bGlobal, false);
    LSTATUS lRet = rs.Open(szRegSubKey, false);
    if (lRet == ERROR_SUCCESS) {
        lRet = rs.ReadValue(szRegValue, prop);
        rs.Close();
    }

    return (lRet == ERROR_SUCCESS) || (lRet == ERROR_FILE_NOT_FOUND);
}

template<typename TSetting>
bool CRegSetting::WriteOne(bool bGlobal, PCTSTR szRegSubKey, PCTSTR szRegValue, TSetting& prop)
{
    CRegSetting rs(bGlobal);
    LSTATUS lRet = rs.Open(szRegSubKey, true);
    if (lRet == ERROR_SUCCESS) {
        lRet = rs.WriteValue(szRegValue, prop);
        rs.Close();
    }

    return (lRet == ERROR_SUCCESS);
}

}

#endif // __WTL_SETTINGSSTORE_H__
