// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_TABLIST_H__
#define __WTL_TABLIST_H__

#include <atlapp.h>
#include <atlbase.h>
#include <atlcrack.h>
#include <atlctrls.h>
#include <atlframe.h>
#include <atlstr.h>
#include <atltypes.h>
#include <atlwin.h>
#include <tchar.h>

#include "wtlextdef.h"

namespace WTL {

template<typename T, typename TBase = CListBox, typename WinTraits = ATL::CControlWinTraits>
class ATL_NO_VTABLE CTabListImpl : public ATL::CWindowImpl<T, TBase, WinTraits>, public COwnerDraw<T> {
    typedef ATL::CWindowImpl<T, TBase, WinTraits> baseClass;

public:
#if (_MSC_VER >= 1923)
    DECLARE_WND_SUPERCLASS2(nullptr, T, TBase::GetWndClassName())
#else
    DECLARE_WND_SUPERCLASS(NULL, TBase::GetWndClassName())
#endif

public:
    BEGIN_MSG_MAP(CTabListImpl)
    MSG_WM_CREATE(OnCreate)
    MSG_WM_ERASEBKGND(OnEraseBkgnd)
    MSG_WM_PAINT(OnPaint)
    MESSAGE_HANDLER_EX(WM_VSCROLL, OnVScroll)
    MESSAGE_HANDLER_EX(LB_SETCURSEL, OnSetCurSel)
    // Note: relies on parent not handling the message
    REFLECTED_COMMAND_CODE_HANDLER_EX(LBN_SELCHANGE, OnSelchange)
    CHAIN_MSG_MAP_ALT(COwnerDraw<T>, 1)
    END_MSG_MAP()

    CTabListImpl()
        : font_()
        , xMargin_(0)
        , focusedItem_(-1)
    {
    }

    void OnFinalMessage(HWND /*thisWnd*/) _WTL_OVERRIDE
    {
        font_ = NULL;
    }

    BOOL SubclassWindow(HWND hWnd)
    {
        BOOL success = baseClass::SubclassWindow(hWnd);
        if (success)
            _Init();
        return success;
    }

    int OnCreate(LPCREATESTRUCT /*createStruct*/)
    {
        _Init();
        this->SetMsgHandled(false);
        return 0;
    }

    BOOL OnEraseBkgnd(CDCHandle /*hdc*/)
    {
        // Erase in OnPaint()

        return true;
    }

    void OnPaint(CDCHandle);

    LRESULT OnVScroll(UINT msg, WPARAM wParam, LPARAM lParam);

    void OnSelchange(UINT /*notifyCode*/, int /*controlId*/, CWindow /*ctlWnd*/)
    {
        this->RedrawWindow();
    }

    LRESULT OnSetCurSel(UINT msg, WPARAM wParam, LPARAM lParam)
    {
        LRESULT r = this->DefWindowProc(msg, wParam, lParam);
        this->RedrawWindow();
        return r;
    }

    // COwnerDraw

    void DrawItem(LPDRAWITEMSTRUCT drawItem);

protected:
    void _Init();

    // static virtual methods

    HFONT MakeFont() const;

    SIZE CalculateMargin(const TEXTMETRIC& tm) const;

protected:
    CFont font_;
    int xMargin_;
    int focusedItem_;
};

// template in order to allow derivation and overriding static virtual methods
template<typename T>
class ATL_NO_VTABLE CTabListBoxT : public CTabListImpl<T, CListBox, ATL::CWinTraitsOR<LBS_OWNERDRAWFIXED | LBS_HASSTRINGS>> {
public:
#if (_MSC_VER >= 1923)
    DECLARE_WND_SUPERCLASS2(_T("WtlTabListBox"), T, CListBox::GetWndClassName())
#else
    DECLARE_WND_SUPERCLASS(_T("WtlTabListBox"), CListBox::GetWndClassName())
#endif

    static const UINT ODT_Type = ODT_LISTBOX;
};

// use this if you don't need to make a different font or calculate a different margin
class CTabListBox : public CTabListBoxT<CTabListBox> {};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

namespace WTL {

template<typename T, typename TBase, typename WinTraits>
void CTabListImpl<T, TBase, WinTraits>::_Init()
{
    // Need to set these in resource editor
    // If it's a CComboBox, use CBS_OWNERDRAWFIXED.
    WTLASSERT(this->GetStyle() & LBS_OWNERDRAWFIXED);

    focusedItem_ = -1;
    xMargin_ = 0;

    // Calculating font and text height like CBCGPListBox::GetItemMinHeight()

    font_.Attach(static_cast<T*>(this)->MakeFont());
    this->SetFont(font_, false);

    {
        CClientDC dc(*this);
        dc.SelectFont(font_);

        TEXTMETRIC tm;
        dc.GetTextMetrics(&tm);

        CSize margin = static_cast<T*>(this)->CalculateMargin(tm);
        this->SetItemHeight(0, tm.tmHeight + 2 * margin.cy);
        xMargin_ = margin.cx;
    }
}

template<typename T, typename TBase, typename WinTraits>
HFONT CTabListImpl<T, TBase, WinTraits>::MakeFont() const
{
    CFontHandle listFont = this->GetParent().GetFont();
    LOGFONT lf = {};
    listFont.GetLogFont(&lf);
    lf.lfHeight = LONG(1.25 * lf.lfHeight);

    CFont font;
    font.CreateFontIndirect(&lf);
    return font.Detach();
}

template<typename T, typename TBase, typename WinTraits>
SIZE CTabListImpl<T, TBase, WinTraits>::CalculateMargin(const TEXTMETRIC& tm) const
{
    CWindowDC screenDC(NULL);
    int yMargin = MulDiv(4, screenDC.GetDeviceCaps(LOGPIXELSX), 96);
    SIZE s = {tm.tmAveCharWidth, (tm.tmHeight * 9 / 5 - tm.tmHeight + yMargin) / 2};
    return s;
}

template<typename T, typename TBase, typename WinTraits>
void CTabListImpl<T, TBase, WinTraits>::OnPaint(CDCHandle)
{
    CRect clientRect;
    this->GetClientRect(&clientRect);

    CPaintDC paintDC(*this);
    {
        CRgn rgn;
        rgn.CreateRectRgnIndirect(clientRect);
        paintDC.SelectClipRgn(rgn);
    }

    CMemoryDC memDC(paintDC, paintDC.m_ps.rcPaint);

    // erase
    memDC.FillSolidRect(clientRect, GetSysColor(COLOR_3DFACE));
    // right border
    memDC.FillSolidRect(clientRect.right - 1, clientRect.top, 1, clientRect.bottom, GetSysColor(COLOR_3DSHADOW));

    // redraw all items
    CRect itemRect = clientRect;
    for (UINT i = this->GetTopIndex(), n = this->GetCount(); i < n && itemRect.top <= clientRect.bottom; ++i) {
        int itemHeight = this->GetItemHeight(i);

        itemRect.bottom = itemRect.top + itemHeight;
        UINT style = 0;
        if (focusedItem_ == i)
            style |= ODS_FOCUS;
        if (this->GetSel(i))
            style |= ODS_SELECTED;
        DRAWITEMSTRUCT drawInfo = {T::ODT_Type, UINT(this->GetDlgCtrlID()), i, ODA_DRAWENTIRE, style, *this, memDC,
                                   itemRect,    this->GetItemData(i)};
        DrawItem(&drawInfo);

        itemRect.OffsetRect(0, itemHeight);
    }
}

template<typename T, typename TBase, typename WinTraits>
LRESULT CTabListImpl<T, TBase, WinTraits>::OnVScroll(UINT msg, WPARAM wParam, LPARAM lParam)
{
    this->SetRedraw(false);
    LRESULT r = this->DefWindowProc(msg, wParam, lParam);
    this->SetRedraw(true);
    this->RedrawWindow();
    return r;
}

template<typename T, typename TBase, typename WinTraits>
void CTabListImpl<T, TBase, WinTraits>::DrawItem(LPDRAWITEMSTRUCT drawItem)
{
    if (drawItem->itemState & ODS_FOCUS)
        focusedItem_ = drawItem->itemID;

    if (drawItem->itemID == -1)
        return;

    CDCHandle dc = drawItem->hDC;
    CRect rc = drawItem->rcItem;
    // Never paint selected color for combobox itself (edit part)
    bool isSelected = (drawItem->itemState & (ODS_SELECTED | ODS_COMBOBOXEDIT)) == ODS_SELECTED;
    CBrushHandle bgBrush = GetSysColorBrush(isSelected ? COLOR_3DLIGHT : COLOR_3DFACE);

    // Fill background (taking right border into account)
    rc.right -= 1;
    dc.FillRect(rc, bgBrush);

    // Draw text
    ATL::CString text;
    this->GetText(drawItem->itemID, text);
    dc.SelectFont(this->GetFont());
    dc.SetBkMode(TRANSPARENT);
    dc.SetTextColor(COLOR_BTNTEXT);
    rc.InflateRect(-xMargin_, 0);
    dc.DrawText(text, text.GetLength(), rc, DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS);
}

} // namespace WTL

#endif // __WTL_TABLIST_H__
