// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_FILESTREAM_H__
#define __WTL_FILESTREAM_H__

#include <ObjIdl.h> // IStream
#include <guiddef.h>
#include <ntverp.h>
#include <tchar.h>
#if VER_PRODUCTBUILD >= 9600 // since Windows SDK 8.1
#include <minwindef.h>
#include <winerror.h>
#else
#include <Windows.h>
#endif
#include "wtlextdef.h"

namespace WTL {

/** @short A file stream implementing the IStream interface.
 *  @note This is not a registered COM object, hence can't be instantiated through the COM mechanism.
 *  Rather use CFileStream::OpenFileFor() or CFileStream::GenerateFile().
 */
class CFileStream : public IStream {
private:
    // nullptr_t ctor
    CFileStream(int) throw();

protected:
    CFileStream() throw();
    ~CFileStream();

public:
    /** Modes denoting how to call the CreateFile() Win32 API */
    enum OpenMode { OpenRead, OpenWrite, OpenReadRelaxed, OpenWriteExisting };

    /** @short Open a file for the specified purpose (mode+disposition), using the CreateFile() Win32 API.
     *  @throw In case of an error throws the last Win32 error via WtlThrowLastWin32().
     */
    static IStream* OpenFileFor(const _TCHAR* filePath, OpenMode mode);

    /** @short Open a file in write mode, using the CFileStream::OpenFile(), and set its size.
     *  @note Reserving an amount of bytes for the file known in advance can possibly save a lot of time.
     *  @throw In case of an error throws an error via WtlThrow().
     */
    static IStream* GenerateFile(const _TCHAR* filePath, ULONGLONG nBytes);

    // IUnknown
public:
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void** object) _WTL_OVERRIDE;
    ULONG STDMETHODCALLTYPE AddRef() _WTL_OVERRIDE;
    ULONG STDMETHODCALLTYPE Release() _WTL_OVERRIDE;

    // ISequentialStream
public:
    HRESULT STDMETHODCALLTYPE Read(void* data, ULONG nBytes, ULONG* nRead) _WTL_OVERRIDE;
    HRESULT STDMETHODCALLTYPE Write(const void* data, ULONG nBytes, ULONG* nWritten) _WTL_OVERRIDE;

    // IStream
public:
    HRESULT STDMETHODCALLTYPE SetSize(ULARGE_INTEGER) _WTL_OVERRIDE;
    HRESULT STDMETHODCALLTYPE CopyTo(IStream*, ULARGE_INTEGER, ULARGE_INTEGER*, ULARGE_INTEGER*) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Commit(DWORD) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Revert() _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE LockRegion(ULARGE_INTEGER, ULARGE_INTEGER, DWORD) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE UnlockRegion(ULARGE_INTEGER, ULARGE_INTEGER, DWORD) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Clone(IStream**) _WTL_OVERRIDE
    {
        return E_NOTIMPL;
    }
    HRESULT STDMETHODCALLTYPE Seek(LARGE_INTEGER distance, DWORD origin, ULARGE_INTEGER* newPosition) _WTL_OVERRIDE;
    HRESULT STDMETHODCALLTYPE Stat(STATSTG* stats, DWORD flags) _WTL_OVERRIDE;

private:
    // note: alignment as documented on MSDN for InterlockedIncrement
    __declspec(align(32)) LONG ref_;

protected:
    HANDLE hFile_;
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <Windows.h>
#include <intsafe.h>

namespace WTL {

inline CFileStream::CFileStream(int) throw()
    : ref_(0)
    , hFile_(NULL)
{
}

inline CFileStream::CFileStream() throw()
    : ref_(0)
    , hFile_(NULL)
{
}

inline CFileStream::~CFileStream()
{
    if (hFile_ && hFile_ != INVALID_HANDLE_VALUE)
        CloseHandle(hFile_);
}

inline IStream* CFileStream::OpenFileFor(const _TCHAR* filePath, OpenMode mode)
{
    CFileStream* stream = _ATL_NEW CFileStream(NULL);
#ifndef _ATL_DISABLE_NOTHROW_NEW
    if (!stream)
        WtlThrow(E_OUTOFMEMORY);
#endif

    DWORD accessMode;
    DWORD shareMode = FILE_SHARE_READ;
    DWORD disposition;
    switch (mode) {
    case OpenRead:
    case OpenReadRelaxed:
        accessMode = GENERIC_READ;
        disposition = OPEN_EXISTING;
        break;
    case OpenWrite:
        accessMode = GENERIC_WRITE;
        disposition = CREATE_ALWAYS;
        break;
    case OpenWriteExisting:
        accessMode = GENERIC_WRITE;
        disposition = OPEN_ALWAYS;
    }
    switch (mode) {
    case OpenReadRelaxed:
        shareMode |= FILE_SHARE_WRITE;
        break;
    case OpenRead:
    case OpenWrite:
    case OpenWriteExisting:
        break;
    }

    stream->hFile_ = CreateFile(filePath, accessMode, shareMode, NULL, disposition, FILE_ATTRIBUTE_NORMAL, NULL);

    if (stream->hFile_ == INVALID_HANDLE_VALUE) {
        delete stream;
        WtlThrowLastWin32();
    }

    return stream;
}

inline IStream* CFileStream::GenerateFile(const _TCHAR* filePath, ULONGLONG nBytes)
{
    IStream* stream = OpenFileFor(filePath, OpenWrite);

    ULARGE_INTEGER uli = {LODWORD(nBytes), HIDWORD(nBytes)};
    HRESULT hr = stream->SetSize(uli);
    if (FAILED(hr)) {
        delete stream;
        WtlThrow(hr);
    }

    return stream;
}

inline HRESULT STDMETHODCALLTYPE CFileStream::QueryInterface(REFIID iid, void** object)
{
    if ((iid == __uuidof(IUnknown)) || (iid == __uuidof(IStream)) || (iid == __uuidof(ISequentialStream))) {
        *object = this;
        AddRef();
        return S_OK;
    }

    return E_NOINTERFACE;
}

inline ULONG STDMETHODCALLTYPE CFileStream::AddRef()
{
    return InterlockedIncrement(&ref_);
}

inline ULONG STDMETHODCALLTYPE CFileStream::Release()
{
    LONG n = InterlockedDecrement(&ref_);
    if (n == 0)
        delete this;
    return n;
}

inline HRESULT STDMETHODCALLTYPE CFileStream::Read(void* data, ULONG nBytes, ULONG* nRead)
{
    BOOL rc = ReadFile(hFile_, data, nBytes, nRead, NULL);
    return rc ? S_OK : HRESULT_FROM_WIN32(GetLastError());
}

inline HRESULT STDMETHODCALLTYPE CFileStream::Write(const void* data, ULONG nBytes, ULONG* nWritten)
{
    BOOL rc = WriteFile(hFile_, data, nBytes, nWritten, NULL);
    return rc ? S_OK : HRESULT_FROM_WIN32(GetLastError());
}

inline HRESULT STDMETHODCALLTYPE CFileStream::SetSize(ULARGE_INTEGER uli)
{
    _COM_ASSERT(uli.QuadPart <= LLONG_MAX);
    if (uli.QuadPart > LLONG_MAX)
        return E_INVALIDARG;

    LARGE_INTEGER li = {uli.LowPart, LONG(uli.HighPart)};
    HRESULT hr = Seek(li, STREAM_SEEK_SET, NULL);
    if (FAILED(hr))
        return hr;

    // now extend or shrink the file;
    // if this is a newly created file then it gets zero-filled if extended
    if (!SetEndOfFile(hFile_))
        return HRESULT_FROM_WIN32(GetLastError());

    return Seek(LARGE_INTEGER(), STREAM_SEEK_SET, NULL);
}

inline HRESULT STDMETHODCALLTYPE CFileStream::Seek(LARGE_INTEGER distance, DWORD origin, ULARGE_INTEGER* newPosition)
{
    switch (origin) {
    case STREAM_SEEK_SET:
    case STREAM_SEEK_CUR:
    case STREAM_SEEK_END:
        break;
    default:
        return STG_E_INVALIDFUNCTION;
    }

    if (!SetFilePointerEx(hFile_, distance, PLARGE_INTEGER(newPosition), origin))
        return HRESULT_FROM_WIN32(GetLastError());
    return S_OK;
}

inline HRESULT STDMETHODCALLTYPE CFileStream::Stat(STATSTG* stats, DWORD)
{
    if (!GetFileSizeEx(hFile_, PLARGE_INTEGER(&stats->cbSize)))
        return HRESULT_FROM_WIN32(GetLastError());
    return S_OK;
}

} // namespace WTL

#endif // __WTL_FILESTREAM_H__
