// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTLEXTDEF_H__
#define __WTLEXTDEF_H__

#include <atldef.h>

#ifdef WTL_NDEBUG_ASSERT
#include <assert.h>
#define WTLASSERT assert
#else
#define WTLASSERT ATLASSERT
#endif

// This macro exists to facilitate throwing custom errors even in MFC projects
#ifndef WtlThrow
#define WtlThrow AtlThrow
#endif

// Throw a CAtlException (or custom) corresponding to the result of GetLastError()
ATL_NOINLINE __declspec(noreturn) inline void WINAPI WtlThrowLastWin32()
{
    DWORD dwError = GetLastError();
    WtlThrow(HRESULT_FROM_WIN32(dwError));
}

#ifndef WTL_NTDDI_VERSION
#define WTL_NTDDI_VERSION NTDDI_VERSION
#endif

#if _MSC_VER >= 1700
#define _WTL_OVERRIDE override
#else
#define _WTL_OVERRIDE
#endif

#if __cpp_decltype_auto >= 201304L
#define WTL_DECLTYPE_AUTO_SUPPORTED
#endif

#endif // __WTLEXTDEF__
