// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_SECTIONGROUPBOX_H__
#define __WTL_SECTIONGROUPBOX_H__

#include <atlapp.h>
#include <atlbase.h>
#include <atlcrack.h>
#include <atlctrls.h>
#include <atlstr.h>
#include <atltypes.h>
#include <atlwin.h>
#include <tchar.h>

#include "wtlextdef.h"

namespace WTL {

template<typename T>
class ATL_NO_VTABLE CSectionGroupBoxT : public ATL::CWindowImpl<T, CButton, ATL::CWinTraitsOR<BS_GROUPBOX>> {
    typedef ATL::CWindowImpl<T, CButton, ATL::CWinTraitsOR<BS_GROUPBOX>> baseClass;

public:
#if (_MSC_VER >= 1923)
    DECLARE_WND_SUPERCLASS2(_T("WtlSectionGroupBox"), T, CButton::GetWndClassName())
#else
    DECLARE_WND_SUPERCLASS(_T("WtlSectionGroupBox"), CButton::GetWndClassName())
#endif

public:
    BEGIN_MSG_MAP(CSectionGroupBoxT)
    MSG_WM_CREATE(OnCreate)
    MSG_WM_PAINT(OnPaint)
    END_MSG_MAP()

    void OnFinalMessage(HWND /*thisWnd*/) _WTL_OVERRIDE
    {
        font_ = NULL;
    }

    BOOL SubclassWindow(HWND hWnd)
    {
        BOOL r = baseClass::SubclassWindow(hWnd);
        if (r)
            _Init();
        return r;
    }

    int OnCreate(LPCREATESTRUCT /*createStruct*/)
    {
        _Init();
        this->SetMsgHandled(false);
        return 0;
    }

    void OnPaint(CDCHandle hdc);

protected:
    void _Init();

    // static virtual methods

    HFONT MakeFont() const;

protected:
    CFont font_;
};

// use this if you don't need to make a different font
class CSectionGroupBox : public CSectionGroupBoxT<CSectionGroupBox> {};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <atlgdi.h>

namespace WTL {

template<typename T>
void CSectionGroupBoxT<T>::OnPaint(CDCHandle hdc)
{
    CPaintDC dc(*this);

    CRect ctrlRect;
    this->GetClientRect(ctrlRect);

    dc.SelectFont(this->GetFont());
    CSize textExtent;
    ATL::CString s;
    this->GetWindowText(s);
    if (s.IsEmpty())
        dc.GetTextExtent(L"A", 1, &textExtent);
    else
        dc.GetTextExtent(s, s.GetLength(), &textExtent);

    if (!s.IsEmpty()) {
        int xMargin = textExtent.cy / 2;
        CRect textRect(0, 0, textExtent.cx, textExtent.cy);
        textRect.InflateRect(xMargin, 0);
        textRect.MoveToXY(ctrlRect.left + xMargin, ctrlRect.top);
        dc.SetBkMode(TRANSPARENT);
        dc.SetTextColor(COLOR_BTNTEXT);
        dc.DrawText(s, s.GetLength(), textRect, DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP);
        dc.ExcludeClipRect(textRect);
    }

    CRect lineRect(ctrlRect.left, ctrlRect.top + textExtent.cy / 2, ctrlRect.right, ctrlRect.top + textExtent.cy / 2 + 1);
    dc.FillRect(lineRect, GetSysColorBrush(COLOR_3DLIGHT));
}

template<typename T>
void CSectionGroupBoxT<T>::_Init()
{
    font_.Attach(static_cast<T*>(this)->MakeFont());
    this->SetFont(font_, false);
}

template<typename T>
HFONT CSectionGroupBoxT<T>::MakeFont() const
{
    CFontHandle listFont = this->GetParent().GetFont();
    LOGFONT lf = {};
    listFont.GetLogFont(&lf);
    lf.lfWeight = FW_SEMIBOLD;

    CFont font;
    font.CreateFontIndirect(&lf);
    return font.Detach();
}

} // namespace WTL

#endif // __WTL_SECTIONGROUPBOX_H__
