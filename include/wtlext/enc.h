// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_ENC_H__
#define __WTL_ENC_H__

#include <atlenc.h>
#include <atlutil.h>
#include <stddef.h>

#include "wtlextdef.h"

namespace WTL {

#define WTL_PERCENT_DECODE ATL_URL_DECODE // Convert %XX escape sequences to characters

#define WTL_MAX_ENC_BUFFER 2048

// Determine if the character is unsafe,
// i.e. a control character or above ASCII or matching a caller-supplied character
template<size_t N>
bool WtlIsPotentiallyUnsafeChar(_In_ char chIn, const char (&callerAscii)[N]) throw()
{
    unsigned char ch = (unsigned char)chIn;
    if (ch < 32 || ch > 126)
        return true;
    for (size_t i = 0, n = N && !callerAscii[N - 1] ? N - 1 : N; i < n; ++i) {
        if (callerAscii[i] == chIn)
            return true;
    }

    return false;
}

// Convert all unsafe characters in szStringIn to escape sequences
// lpszStringIn and lpszStringOut should be different strings
template<size_t N>
_Success_(return == true) bool AtlPercentEncode(_In_z_ LPCSTR szStringIn, _Out_writes_to_(dwMaxLength, *pdwStrLen) LPSTR szStringOut,
                                                _Out_opt_ DWORD* pdwStrLen, _In_ DWORD dwMaxLength, _In_ const char (&callerAscii)[N],
                                                _In_ DWORD dwFlags = 0)
{
    ATLENSURE(szStringIn != NULL);
    ATLENSURE(szStringOut != NULL);
    ATLENSURE(szStringIn != szStringOut);

    char ch;
    DWORD dwLen = 0;
    bool bRet = true;
    DWORD dwFlagsInternal = dwFlags;
    // The next 2 are for buffer security checks
    LPSTR szOrigStringOut = szStringOut;
    LPSTR szStringOutEnd = (szStringOut + dwMaxLength);

    while ((ch = *szStringIn++) != '\0') {
        // if we are at the maximum length, set bRet to false
        // this ensures no more data is written to szStringOut, but
        // the length of the string is still updated, so the user
        // knows how much space to allocate
        if (dwLen == dwMaxLength) {
            bRet = false;
        }

        else if (ch == '%' && (dwFlagsInternal & WTL_PERCENT_DECODE)) {
            // decode the escaped sequence
            if (*szStringIn != '\0') {
                short nFirstDigit = AtlHexValue(*szStringIn++);

                if (nFirstDigit < 0) {
                    bRet = false;
                    break;
                }
                ch = static_cast<char>(16 * nFirstDigit);
                if (*szStringIn != '\0') {
                    short nSecondDigit = AtlHexValue(*szStringIn++);

                    if (nSecondDigit < 0) {
                        bRet = false;
                        break;
                    }
                    ch = static_cast<char>(ch + nSecondDigit);
                }
                else {
                    break;
                }
            }
            else {
                break;
            }
        }

        // if we are encoding and it is an unsafe character
        if (WtlIsPotentiallyUnsafeChar(ch, callerAscii)) {
            {
                // if there is not enough space for the escape sequence
                if (dwLen >= (dwMaxLength - 3)) {
                    bRet = false;
                }
                if (bRet) {
                    // output the percent, followed by the hex value of the character
                    LPSTR pszTmp = szStringOut;
                    *pszTmp++ = '%';
                    if ((unsigned char)ch < 16) {
                        *pszTmp++ = '0';
                    }
                    ATL::Checked::ultoa_s((unsigned char)ch, pszTmp, szStringOutEnd - pszTmp, 16);
                    szStringOut += sizeof("%FF") - 1;
                }
                dwLen += sizeof("%FF") - 2;
            }
        }
        else // safe character
        {
            if (bRet)
                *szStringOut++ = ch;
        }
        dwLen++;
    }

    if (bRet && dwLen < dwMaxLength)
        *szStringOut = '\0';

    if (pdwStrLen)
        *pdwStrLen = dwLen + 1;

    if (dwLen + 1 > dwMaxLength)
        bRet = false;

    return bRet;
}

template<size_t N>
_Success_(return == true) bool AtlPercentEncode(_In_z_ LPCWSTR szStringIn, _Out_writes_to_(dwMaxLength, *pdwStrLen) LPWSTR szStringOut,
                                                _Out_opt_ DWORD* pdwStrLen, _In_ DWORD dwMaxLength, const char (&callerAscii)[N],
                                                _In_ DWORD dwFlags = 0)
{
    ATLENSURE(szStringIn != NULL);
    ATLENSURE(szStringOut != NULL);
    // convert to UTF8
    bool bRet = false;

    int nSrcLen = ATL::AtlStrLen(szStringIn);
    if (nSrcLen == 0) // handle the case of an empty string
    {
        if (pdwStrLen != NULL) {
            *pdwStrLen = 1; // one for null
        }
        *szStringOut = '\0';
        return true;
    }
    int nCnt = ATL::AtlUnicodeToUTF8(szStringIn, nSrcLen, NULL, 0);
    if (nCnt != 0) {
        nCnt++;
        CHeapPtr<char> szIn;

        char szInBuf[WTL_MAX_ENC_BUFFER];
        char* pszIn = szInBuf;

        // try to avoid allocation
        if (nCnt <= 0) {
            return false;
        }

        if (nCnt > WTL_MAX_ENC_BUFFER) {
            if (!szIn.AllocateBytes(nCnt)) {
                // out of memory
                return false;
            }
            pszIn = szIn;
        }

        nCnt = ATL::AtlUnicodeToUTF8(szStringIn, nSrcLen, pszIn, nCnt);
        WTLASSERT(nCnt != 0);

        pszIn[nCnt] = '\0';

        char szOutBuf[WTL_MAX_ENC_BUFFER];
        char* pszOut = szOutBuf;
        CHeapPtr<char> szTmp;

        // try to avoid allocation
        if (dwMaxLength > WTL_MAX_ENC_BUFFER) {
            if (!szTmp.AllocateBytes(dwMaxLength)) {
                // out of memory
                return false;
            }
            pszOut = szTmp;
        }

        DWORD dwStrLen = 0;
        bRet = AtlPercentEncode(pszIn, pszOut, &dwStrLen, dwMaxLength, callerAscii, dwFlags);
        if (bRet) {
            // it is now safe to convert using any codepage, since there
            // are no non-ASCII characters
            _ATLTRY
            {
                ATL::Checked::wmemcpy_s(szStringOut, dwMaxLength, CA2W(pszOut), dwStrLen);
            }
            _ATLCATCHALL()
            {
                bRet = false;
            }
        }
        if (pdwStrLen) {
            *pdwStrLen = dwStrLen;
        }
    }

    return bRet;
}

// Convert all escaped characters in szString to their real values
// lpszStringIn and lpszStringOut can be the same string
_Success_(return == true) inline bool AtlPercentDecode(_In_z_ LPCSTR szStringIn,
                                                       _Out_writes_to_(dwMaxLength, *pdwStrLen) LPSTR szStringOut,
                                                       _Out_opt_ _Always_(_When_(pdwStrLen != NULL, _Post_valid_)) LPDWORD pdwStrLen,
                                                       _In_ DWORD dwMaxLength)
{
    return AtlUnescapeUrl(szStringIn, szStringOut, pdwStrLen, dwMaxLength) || false;
}

_Success_(return == true) inline bool AtlPercentDecode(_In_z_ LPCWSTR szStringIn,
                                                       _Out_writes_to_(dwMaxLength, *pdwStrLen) LPWSTR szStringOut,
                                                       _Out_opt_ LPDWORD pdwStrLen, _In_ DWORD dwMaxLength)
{
    return AtlUnescapeUrl(szStringIn, szStringOut, pdwStrLen, dwMaxLength) || false;
}

/** @short Convenience method around `ATL::EscapeXML()` saving the trouble of preallocating the string buffer.
 *  @param dwFlags One of `ATL_ESC_FLAG_NONE`, `ATL_ESC_FLAG_ATTR`
 */
template<typename Traits>
ATL::CStringT<wchar_t, Traits> EscapeXML(_In_ const ATL::CStringT<wchar_t, Traits>& strIn, _In_ DWORD dwFlags);

#ifdef WTL_DECLTYPE_AUTO_SUPPORTED
/** @short Convenience method around `ATL::EscapeXML()` saving the trouble of preallocating the string buffer.
 *  @param dwFlags One of `ATL_ESC_FLAG_NONE`, `ATL_ESC_FLAG_ATTR`
 *  @return `CStringW` if `_AFX` is defined, `ATL::CStringW` otherwise.
 */
inline auto EscapeXML(_In_ const wchar_t* szIn, _In_ DWORD dwFlags);

/** @short Convenience method around `ATL::EscapeXML()` saving the trouble of preallocating the string buffer.
 *  @param dwFlags One of `ATL_ESC_FLAG_NONE`, `ATL_ESC_FLAG_ATTR`
 *  @return `CStringW` if `_AFX` is defined, `ATL::CStringW` otherwise.
 */
inline auto EscapeXML(_In_ const wchar_t* szIn, _In_ int inLength,
                      // one of `ATL_ESC_FLAG_NONE`, `ATL_ESC_FLAG_ATTR`
                      _In_ DWORD flags);
#endif

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <atlstr.h>

namespace WTL {

template<typename StringTraits>
ATL::CStringT<wchar_t, Traits> EscapeXML(_In_ const ATL::CStringT<wchar_t, StringTraits>& strIn, _In_ DWORD dwFlags)
{
    ATL::CStringT<wchar_t, StringTraits> escaped;
    int needed = ATL::EscapeXML(strIn, strIn.GetLength(), nullptr, 0, dwFlags);
    needed = ATL::EscapeXML(strIn, strIn.GetLength(), escaped.GetBufferSetLength(needed), needed, dwFlags);
    return escaped;
}

#ifdef WTL_DECLTYPE_AUTO_SUPPORTED
inline auto EscapeXML(_In_ const wchar_t* szIn, _In_ DWORD dwFlags)
{
#ifndef _AFX
    using ATL::CStringW;
#endif

    CStringW escaped;
    int len = CStringW::StringLength(szIn);
    int needed = ATL::EscapeXML(szIn, len, nullptr, 0, dwFlags);
    needed = ATL::EscapeXML(szIn, len, escaped.GetBufferSetLength(needed), needed, dwFlags);
    return escaped;
}

inline auto EscapeXML(_In_ const wchar_t* szIn, _In_ int nSrcLen, _In_ DWORD dwFlags)
{
#ifndef _AFX
    using ATL::CStringW;
#endif

    CStringW escaped;
    int needed = ATL::EscapeXML(szIn, nSrcLen, nullptr, 0, dwFlags);
    needed = ATL::EscapeXML(szIn, nSrcLen, escaped.GetBufferSetLength(needed), needed, dwFlags);
    return escaped;
}
#endif

} // namespace WTL

#endif // __WTL_ENC_H__
