// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_INET_H__
#define __WTL_INET_H__

#include <atlbase.h>
#include <atlstr.h>
#ifndef _WININET_
#include <WinInet.h>
#endif
#pragma comment(lib, "WinInet.lib")

#if !defined(__ATLCOLL_H__)
#error inet.h requires CAtlMap (from ATL's atlcoll.h)
#endif

#if !defined(__ATLTRACE_H__)
#error inet.h requires ATLTRACE (from ATL's atltrace.h)
#endif

namespace WTL {

BOOL AtlParseURL(LPCTSTR pstrURL, DWORD& dwServiceType, ATL::CString& strServer, ATL::CString& strObject, INTERNET_PORT& nPort);

DWORD AtlGetInternetHandleType(HINTERNET hQuery);

void AtlThrowInternetException(DWORD_PTR dwContext, DWORD dwError = 0);

// these are defined by WININET.H

#define WTL_INET_SERVICE_FTP INTERNET_SERVICE_FTP
#define WTL_INET_SERVICE_HTTP INTERNET_SERVICE_HTTP
#define WTL_INET_SERVICE_GOPHER INTERNET_SERVICE_GOPHER

// these are types that WTL parsing functions understand

#define WTL_INET_SERVICE_UNK 0x1000
#define WTL_INET_SERVICE_FILE (WTL_INET_SERVICE_UNK + 1)
#define WTL_INET_SERVICE_MAILTO (WTL_INET_SERVICE_UNK + 2)
#define WTL_INET_SERVICE_MID (WTL_INET_SERVICE_UNK + 3)
#define WTL_INET_SERVICE_CID (WTL_INET_SERVICE_UNK + 4)
#define WTL_INET_SERVICE_NEWS (WTL_INET_SERVICE_UNK + 5)
#define WTL_INET_SERVICE_NNTP (WTL_INET_SERVICE_UNK + 6)
#define WTL_INET_SERVICE_PROSPERO (WTL_INET_SERVICE_UNK + 7)
#define WTL_INET_SERVICE_TELNET (WTL_INET_SERVICE_UNK + 8)
#define WTL_INET_SERVICE_WAIS (WTL_INET_SERVICE_UNK + 9)
#define WTL_INET_SERVICE_AFS (WTL_INET_SERVICE_UNK + 10)
#define WTL_INET_SERVICE_HTTPS (WTL_INET_SERVICE_UNK + 11)

DECLARE_TRACE_CATEGORY(atlTraceInternet);
#ifdef _DEBUG
__declspec(selectany) ATL::CTraceCategory atlTraceInternet(_T("atlTraceInternet"));
#endif // _DEBUG

class CInternetConnection;
class CHttpConnection;
class CInternetFile;

class CInternetException : public ATL::CAtlException {
public:
    // Constructor
    CInternetException(DWORD dwError, DWORD_PTR dwContext);

    // Attributes
    DWORD m_dwError;
    DWORD_PTR m_dwContext;

    // Implementation
public:
    ~CInternetException();
    CString GetErrorMessage() const;
};

class CInternetSession {
public:
    explicit CInternetSession(LPCTSTR pstrAgent = NULL, DWORD_PTR dwContext = 1, DWORD dwAccessType = PRE_CONFIG_INTERNET_ACCESS,
                              LPCTSTR pstrProxyName = NULL, LPCTSTR pstrProxyBypass = NULL, DWORD dwFlags = 0);

    BOOL QueryOption(DWORD dwOption, LPVOID lpBuffer, LPDWORD lpdwBufLen) const;
    BOOL QueryOption(DWORD dwOption, DWORD& dwValue) const;

    BOOL SetOption(DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength, DWORD dwFlags = 0);
    BOOL SetOption(DWORD dwOption, DWORD dwValue, DWORD dwFlags = 0);

    CInternetFile* OpenURL(LPCTSTR pstrURL, DWORD_PTR dwContext = 1, DWORD dwFlags = INTERNET_FLAG_TRANSFER_ASCII,
                           LPCTSTR pstrHeaders = NULL, DWORD dwHeadersLength = 0);

    CHttpConnection* GetHttpConnection(LPCTSTR pstrServer, INTERNET_PORT nPort = INTERNET_INVALID_PORT_NUMBER,
                                       LPCTSTR pstrUserName = NULL, LPCTSTR pstrPassword = NULL);
    CHttpConnection* GetHttpConnection(LPCTSTR pstrServer, DWORD dwFlags, INTERNET_PORT nPort = INTERNET_INVALID_PORT_NUMBER,
                                       LPCTSTR pstrUserName = NULL, LPCTSTR pstrPassword = NULL);

    BOOL EnableStatusCallback(BOOL bEnable = TRUE);

    // Operations

    DWORD_PTR GetContext() const;
    operator HINTERNET() const;
    virtual void Close();

    // cookies
    static BOOL SetCookie(LPCTSTR pstrUrl, LPCTSTR pstrCookieName, LPCTSTR pstrCookieData);
    static BOOL GetCookie(_In_z_ LPCTSTR pstrUrl, _In_z_ LPCTSTR pstrCookieName, _Out_writes_z_(dwBufLen) LPTSTR pstrCookieData,
                          _In_ DWORD dwBufLen);
    static DWORD GetCookieLength(LPCTSTR pstrUrl, LPCTSTR pstrCookieName);
    static BOOL GetCookie(LPCTSTR pstrUrl, LPCTSTR pstrCookieName, ATL::CString& strCookieData);

    // Overridables
    virtual void OnStatusCallback(DWORD_PTR dwContext, DWORD dwInternetStatus, LPVOID lpvStatusInformation,
                                  DWORD dwStatusInformationLength);

    // Implementation
    ~CInternetSession();

protected:
    DWORD_PTR m_dwContext;
    HINTERNET m_hSession;
    INTERNET_STATUS_CALLBACK m_pOldCallback;
    BOOL m_bCallbackEnabled;
};

class CInternetFile {
    friend class CInternetSession;
    friend class CHttpConnection;

public:
    enum SeekPosition { begin = FILE_BEGIN, current = FILE_CURRENT, end = FILE_END };

    // Constructors
protected:
    CInternetFile(HINTERNET hFile, LPCTSTR pstrFileName, CInternetConnection* pConnection, BOOL bReadMode);
    CInternetFile(HINTERNET hFile, HINTERNET hSession, LPCTSTR pstrFileName, LPCTSTR pstrServer, DWORD_PTR dwContext, BOOL bReadMode);

    // Attributes
protected:
    HINTERNET m_hFile;

public:
    operator HINTERNET() const;
    DWORD_PTR GetContext() const;

    // Operations
    BOOL SetWriteBufferSize(UINT nWriteSize);
    BOOL SetReadBufferSize(UINT nReadSize);

    BOOL QueryOption(DWORD dwOption, LPVOID lpBuffer, LPDWORD lpdwBufLen) const;
    BOOL QueryOption(DWORD dwOption, DWORD& dwValue) const;

    BOOL SetOption(DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength, DWORD dwFlags = 0);
    BOOL SetOption(DWORD dwOption, DWORD dwValue, DWORD dwFlags = 0);

    // Overridables
    virtual ULONGLONG Seek(LONGLONG lOffset, UINT nFrom);

    virtual UINT Read(void* lpBuf, UINT nCount);
    virtual void Write(const void* lpBuf, UINT nCount);

    virtual void Abort();
    virtual void Flush();

    virtual void Close();
    virtual ULONGLONG GetLength() const;

    virtual BOOL ReadString(ATL::CString& rString);
    virtual LPTSTR ReadString(_Out_writes_z_(nMax) LPTSTR pstr, _In_ UINT nMax);
    virtual void WriteString(LPCTSTR pstr);

    // Not supported by CInternetFile
    void LockRange(ULONGLONG dwPos, ULONGLONG dwCount);
    void UnlockRange(ULONGLONG dwPos, ULONGLONG dwCount);
    CInternetFile* Duplicate() const;
    virtual void SetLength(ULONGLONG dwNewLen);

    // Implementation
public:
    virtual ~CInternetFile();

protected:
    BOOL m_bReadMode;
    DWORD_PTR m_dwContext;
    HINTERNET m_hConnection;

    ATL::CString m_strFileName;
    ATL::CString m_strServerName;

    UINT m_nWriteBufferSize;
    UINT m_nWriteBufferPos;
    LPBYTE m_pbWriteBuffer;

    UINT m_nReadBufferSize;
    UINT m_nReadBufferPos;
    LPBYTE m_pbReadBuffer;
    UINT m_nReadBufferBytes;
};

class CHttpFile : public CInternetFile {
    friend class CHttpConnection;
    friend class CInternetSession;

    // Constructors
protected:
    CHttpFile(HINTERNET hFile, HINTERNET hSession, LPCTSTR pstrObject, LPCTSTR pstrServer, LPCTSTR pstrVerb, DWORD_PTR dwContext);
    CHttpFile(HINTERNET hFile, LPCTSTR pstrVerb, LPCTSTR pstrObject, CHttpConnection* pConnection);

    // Operations
public:
    BOOL AddRequestHeaders(LPCTSTR pstrHeaders, DWORD dwFlags = HTTP_ADDREQ_FLAG_ADD_IF_NEW, int dwHeadersLen = -1);
    BOOL AddRequestHeaders(ATL::CString& str, DWORD dwFlags = HTTP_ADDREQ_FLAG_ADD_IF_NEW);

    BOOL SendRequest(LPCTSTR pstrHeaders = NULL, DWORD dwHeadersLen = 0, LPVOID lpOptional = NULL, DWORD dwOptionalLen = 0);
    BOOL SendRequest(ATL::CString& strHeaders, LPVOID lpOptional = NULL, DWORD dwOptionalLen = 0);
    BOOL SendRequestEx(DWORD dwTotalLen, DWORD dwFlags = HSR_INITIATE, DWORD_PTR dwContext = 1);
    BOOL SendRequestEx(LPINTERNET_BUFFERS lpBuffIn, LPINTERNET_BUFFERS lpBuffOut, DWORD dwFlags = HSR_INITIATE,
                       DWORD_PTR dwContext = 1);
    BOOL EndRequest(DWORD dwFlags = 0, LPINTERNET_BUFFERS lpBuffIn = NULL, DWORD_PTR dwContext = 1);
    BOOL QueryInfo(DWORD dwInfoLevel, LPVOID lpvBuffer, LPDWORD lpdwBufferLength, LPDWORD lpdwIndex = NULL) const;
    BOOL QueryInfo(DWORD dwInfoLevel, ATL::CString& str, LPDWORD dwIndex = NULL) const;
    BOOL QueryInfo(DWORD dwInfoLevel, SYSTEMTIME* pSysTime, LPDWORD dwIndex = NULL) const;
    BOOL QueryInfo(DWORD dwInfoLevel, DWORD& dwResult, LPDWORD dwIndex = NULL) const;
    BOOL QueryInfoStatusCode(DWORD& dwStatusCode) const;

    DWORD ErrorDlg(HWND hParentWnd = NULL, DWORD dwError = ERROR_INTERNET_INCORRECT_PASSWORD,
                   DWORD dwFlags = FLAGS_ERROR_UI_FLAGS_GENERATE_DATA | FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS, LPVOID* lppvData = NULL);

    // Attributes
public:
    ATL::CString GetVerb() const;

#pragma push_macro("GetObject")
#undef GetObject
    ATL::CString GetObject() const;
#pragma pop_macro("GetObject")

    virtual ATL::CString GetFileURL() const;

    // Implementation
public:
    virtual ~CHttpFile();

protected:
    ATL::CString m_strObject;
    ATL::CString m_strVerb;
};

/*============================================================================*/
// Connection types

class CInternetConnection {
public:
    CInternetConnection(CInternetSession* pSession, LPCTSTR pstrServer, INTERNET_PORT nPort = INTERNET_INVALID_PORT_NUMBER,
                        DWORD_PTR dwContext = 1);

    // Operations
    operator HINTERNET() const;
    DWORD_PTR GetContext() const;
    CInternetSession* GetSession() const;

    ATL::CString GetServerName() const;

    BOOL QueryOption(DWORD dwOption, LPVOID lpBuffer, LPDWORD lpdwBufLen) const;
    BOOL QueryOption(DWORD dwOption, DWORD& dwValue) const;

    BOOL SetOption(DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength, DWORD dwFlags = 0);
    BOOL SetOption(DWORD dwOption, DWORD dwValue, DWORD dwFlags = 0);

    virtual void Close();

    // Implementation
protected:
    HINTERNET m_hConnection;
    DWORD_PTR m_dwContext;
    CInternetSession* m_pSession;

    ATL::CString m_strServerName;
    INTERNET_PORT m_nPort;

public:
    ~CInternetConnection();
};

class CHttpConnection : public CInternetConnection {
public:
    enum {
        _HTTP_VERB_MIN = 0,
        HTTP_VERB_POST = 0,
        HTTP_VERB_GET = 1,
        HTTP_VERB_HEAD = 2,
        HTTP_VERB_PUT = 3,
        HTTP_VERB_LINK = 4,
        HTTP_VERB_DELETE = 5,
        HTTP_VERB_UNLINK = 6,
        _HTTP_VERB_MAX = 6,
    };

public:
    CHttpConnection(CInternetSession* pSession, HINTERNET hConnected, LPCTSTR pstrServer, DWORD_PTR dwContext);
    CHttpConnection(CInternetSession* pSession, LPCTSTR pstrServer, INTERNET_PORT nPort = INTERNET_INVALID_PORT_NUMBER,
                    LPCTSTR pstrUserName = NULL, LPCTSTR pstrPassword = NULL, DWORD_PTR dwContext = 1);
    CHttpConnection(CInternetSession* pSession, LPCTSTR pstrServer, DWORD dwFlags, INTERNET_PORT nPort = INTERNET_INVALID_PORT_NUMBER,
                    LPCTSTR pstrUserName = NULL, LPCTSTR pstrPassword = NULL, DWORD_PTR dwContext = 1);

    CHttpFile* OpenRequest(LPCTSTR pstrVerb, LPCTSTR pstrObjectName, LPCTSTR pstrReferer = NULL, DWORD_PTR dwContext = 1,
                           LPCTSTR* ppstrAcceptTypes = NULL, LPCTSTR pstrVersion = NULL,
                           DWORD dwFlags = INTERNET_FLAG_EXISTING_CONNECT);

    CHttpFile* OpenRequest(int nVerb, LPCTSTR pstrObjectName, LPCTSTR pstrReferer = NULL, DWORD_PTR dwContext = 1,
                           LPCTSTR* ppstrAcceptTypes = NULL, LPCTSTR pstrVersion = NULL,
                           DWORD dwFlags = INTERNET_FLAG_EXISTING_CONNECT);

    // Implementation
    ~CHttpConnection();

protected:
    ATL::CString m_strServerName;
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include "wtlextdef.h"

namespace WTL {

extern __declspec(selectany) CAtlMap<HINTERNET, CInternetSession*> _atlInetSessionMap;

extern const __declspec(selectany) TCHAR _atlURLhttp[] = _T("http://");

extern const __declspec(selectany) LPCTSTR _atlHtmlVerbs[] = {
    _T("POST"), _T("GET"), _T("HEAD"), _T("PUT"), _T("LINK"), _T("DELETE"), _T("UNLINK"),
};

static inline BOOL __internetSetOptionEx(HINTERNET hInternet, DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength, DWORD dwFlags)
{
    BOOL fResult = FALSE;
    if (dwFlags)
        SetLastError(ERROR_INVALID_PARAMETER);
    else
        fResult = InternetSetOption(hInternet, dwOption, lpBuffer, dwBufferLength);
    return fResult;
}

#ifdef _DEBUG
inline void AtlInternetStatusCallbackDebug(HINTERNET hInternet, DWORD_PTR dwContext, DWORD dwInternetStatus,
                                           LPVOID lpvStatusInformation, DWORD dwStatusInformationLength)
{
    ATLTRACE(atlTraceInternet, 1, _T("Internet ctxt=%d: "), dwContext);

    switch (dwInternetStatus) {
    case INTERNET_STATUS_RESOLVING_NAME:
        ATLTRACE(atlTraceInternet, 1, _T("resolving name for %Ts\n"), lpvStatusInformation);
        break;

    case INTERNET_STATUS_NAME_RESOLVED:
        ATLTRACE(atlTraceInternet, 1, _T("resolved name for %Ts!\n"), lpvStatusInformation);
        break;

    case INTERNET_STATUS_HANDLE_CREATED:
        ATLTRACE(atlTraceInternet, 1, _T("handle %8.8X created\n"), hInternet);
        break;

    case INTERNET_STATUS_CONNECTING_TO_SERVER: {
        sockaddr* pSockAddr = (sockaddr*)lpvStatusInformation;
        ATLTRACE(atlTraceInternet, 1, _T("connecting to socket address '%Ts'\n"), pSockAddr->sa_data);
    } break;

    case INTERNET_STATUS_REQUEST_SENT:
        ATLTRACE(atlTraceInternet, 1, _T("request sent!\n"));
        break;

    case INTERNET_STATUS_SENDING_REQUEST:
        ATLTRACE(atlTraceInternet, 1, _T("sending request...\n"));
        break;

    case INTERNET_STATUS_CONNECTED_TO_SERVER:
        ATLTRACE(atlTraceInternet, 1, _T("connected to socket address!\n"));
        break;

    case INTERNET_STATUS_RECEIVING_RESPONSE:
        ATLTRACE(atlTraceInternet, 1, _T("receiving response...\n"));
        break;

    case INTERNET_STATUS_RESPONSE_RECEIVED:
        ATLTRACE(atlTraceInternet, 1, _T("response received!\n"));
        break;

    case INTERNET_STATUS_CLOSING_CONNECTION:
        ATLTRACE(atlTraceInternet, 1, _T("closing connection %8.8X\n"), hInternet);
        break;

    case INTERNET_STATUS_CONNECTION_CLOSED:
        ATLTRACE(atlTraceInternet, 1, _T("connection %8.8X closed!\n"), hInternet);
        break;

    case INTERNET_STATUS_HANDLE_CLOSING:
        ATLTRACE(atlTraceInternet, 1, _T("handle %8.8X closed!\n"), hInternet);
        break;

    case INTERNET_STATUS_REQUEST_COMPLETE:
        if (dwStatusInformationLength == sizeof(INTERNET_ASYNC_RESULT)) {
            INTERNET_ASYNC_RESULT* pResult = (INTERNET_ASYNC_RESULT*)lpvStatusInformation;
            ATLTRACE(atlTraceInternet, 1, _T("request complete, dwResult = %8.8X, dwError = %8.8X\n"), pResult->dwResult,
                     pResult->dwError);
        }
        else
            ATLTRACE(atlTraceInternet, 1, _T("request complete.\n"));
        break;

    case INTERNET_STATUS_CTL_RESPONSE_RECEIVED:
    case INTERNET_STATUS_REDIRECT:
    default:
        ATLTRACE(atlTraceInternet, 1, _T("Unknown status: %d\n"), dwInternetStatus);
        break;
    }
}
#endif // _DEBUG

inline void CALLBACK AtlInternetStatusCallback(HINTERNET hInternet, DWORD_PTR dwContext, DWORD dwInternetStatus,
                                               LPVOID lpvStatusInformation, DWORD dwStatusInformationLength)
{
    CInternetSession* pSession;

#ifdef _DEBUG
    AtlInternetStatusCallbackDebug(hInternet, dwContext, dwInternetStatus, lpvStatusInformation, dwStatusInformationLength);
#endif

    if (_atlInetSessionMap.Lookup(hInternet, pSession)) {
        pSession->OnStatusCallback(dwContext, dwInternetStatus, lpvStatusInformation, dwStatusInformationLength);
    }

    // note that an entry we can't match is simply ignored as
    // WININET can send notifications for handles that we can't
    // see -- such as when using InternetOpenURL()
}

inline ATL::CString CInternetException::GetErrorMessage() const
{
    struct funclocal {
        struct CBData {
            UINT nID;
            WORD wLangID;
            MESSAGE_RESOURCE_ENTRY* entry;
        };

        static HMODULE load_lib(const wchar_t* name)
        {
            HMODULE hLib = LoadLibraryExW(name, NULL, LOAD_LIBRARY_AS_DATAFILE_EXCLUSIVE | LOAD_LIBRARY_AS_IMAGE_RESOURCE);
            if (hLib == NULL) {
                // if library load failed using flags only valid on Vista+, fall back to using flags valid on XP
                hLib = LoadLibraryExW(name, NULL, LOAD_LIBRARY_AS_DATAFILE);
            }

            return hLib;
        }

        static BOOL CALLBACK EnumResLangCB(HINSTANCE hMod, LPCWSTR lpszType, LPCWSTR lpszName, WORD wLangID, LONG_PTR lParam)
        {
            WTLASSERT(lpszType == /*RT_MESSAGETABLE*/ MAKEINTRESOURCEW(11));
            WTLASSERT(lpszName == MAKEINTRESOURCEW(1));

            CBData* cbdata = (CBData*)lParam;
            // keep searching for requested language
            if (PRIMARYLANGID(wLangID) != PRIMARYLANGID(cbdata->wLangID))
                return TRUE;

            if (HRSRC src = FindResourceExW(hMod, /*RT_MESSAGETABLE*/ MAKEINTRESOURCEW(11), MAKEINTRESOURCEW(1), wLangID)) {
                MESSAGE_RESOURCE_DATA* pmrd = PMESSAGE_RESOURCE_DATA(LockResource(LoadResource(hMod, src)));
                // loop through message blocks
                for (DWORD dwBlock = 0; dwBlock < pmrd->NumberOfBlocks; ++dwBlock) {
                    // outside of message id range?
                    if (cbdata->nID < pmrd->Blocks[dwBlock].LowId || cbdata->nID > pmrd->Blocks[dwBlock].HighId)
                        continue;

                    // first entry
                    MESSAGE_RESOURCE_ENTRY* pmre = PMESSAGE_RESOURCE_ENTRY(LPBYTE(pmrd) + pmrd->Blocks[dwBlock].OffsetToEntries);
                    // loop through all entries
                    for (DWORD id = pmrd->Blocks[dwBlock].LowId; id <= pmrd->Blocks[dwBlock].HighId; ++id) {
                        if (id == cbdata->nID) {
                            // check for unicode
                            if (pmre->Flags)
                                cbdata->entry = pmre;
                            return FALSE;
                        }

                        // next entry
                        pmre = PMESSAGE_RESOURCE_ENTRY(LPBYTE(pmre) + pmre->Length);
                    }
                }
            }

            return FALSE;
        }

        static ATL::CStringW load_default_message_string(HMODULE hMod, UINT nID, LANGID wLanguageId)
        {
            CBData cbdata = {nID, wLanguageId, NULL};
            if (!EnumResourceLanguagesW(hMod, /*RT_MESSAGETABLE*/ MAKEINTRESOURCEW(11), MAKEINTRESOURCEW(1), EnumResLangCB,
                                        LPARAM(&cbdata)) &&
                GetLastError() == ERROR_RESOURCE_ENUM_USER_STOP && cbdata.entry) {
                return ATL::CStringW(LPCWSTR(cbdata.entry->Text), cbdata.entry->Length);
            }

            return ATL::CStringW();
        }

        static ATL::CStringW format(const CInternetException* const _this, UINT nID)
        {
            const LANGID sysLangID = GetSystemDefaultLangID();
            ATL::CStringW r;
            HMODULE hWinINet = load_lib(L"WININET.DLL");
            // no FormatMessage:
            // in our effort to avoid fixed-size buffers we get into the whole hassle of loading the strings
            // directly from the system's resource message tables;
            // this is to avoid any unnecessary allocations and load the message string directly into a CStringW
            r = load_default_message_string(hWinINet, nID, sysLangID);
            FreeLibrary(hWinINet);

            if (r.IsEmpty()) {
                // it failed! try Windows...

                HINSTANCE hKernel32 = load_lib(L"KERNEL32.DLL");
                r = load_default_message_string(hKernel32, nID, sysLangID);
                FreeLibrary(hKernel32);
            }

            return r;
        }
    };

    ATL::CString ret;
    if (m_dwError == ERROR_INTERNET_EXTENDED_ERROR) {
        DWORD dwLength = 0;
        DWORD dwError;

        // find the length of the error
        if (!InternetGetLastResponseInfo(&dwError, NULL, &dwLength) && GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
            InternetGetLastResponseInfo(&dwError, ret.GetBuffer(dwLength + 1), &dwLength);
            ret.ReleaseBufferSetLength(dwLength);
        }
        else {
            ATLTRACE(atlTraceInternet, 0, "Warning: Extended error reported with no response info\n");
            ret = funclocal::format(this, m_dwError);
        }
    }
    else
        ret = funclocal::format(this, m_dwError);

    return ret;
}

inline CInternetException::CInternetException(DWORD dwError, DWORD_PTR dwContext)
    : m_dwError(dwError)
    , m_dwContext(dwContext)
{
}

inline CInternetException::~CInternetException() {}

inline CInternetSession::~CInternetSession()
{
    Close();
}

inline CInternetSession::CInternetSession(LPCTSTR pstrAgent /* = NULL */, DWORD_PTR dwContext /* = 1 */,
                                          DWORD dwAccessType /* = PRE_CONFIG_INTERNET_ACCESS */, LPCTSTR pstrProxyName /* = NULL */,
                                          LPCTSTR pstrProxyBypass /* = NULL */, DWORD dwFlags /* = 0 */)
{
    WTLASSERT(ATL::AtlIsValidString(pstrAgent));
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);
    m_bCallbackEnabled = FALSE;
    m_pOldCallback = NULL;

    m_dwContext = dwContext;
    m_hSession = InternetOpen(pstrAgent, dwAccessType, pstrProxyName, pstrProxyBypass, dwFlags);

    if (m_hSession == NULL)
        AtlThrowInternetException(m_dwContext);
    else
        _atlInetSessionMap.SetAt(m_hSession, this);
}

inline void CInternetSession::Close()
{
    if (m_bCallbackEnabled)
        EnableStatusCallback(FALSE);

    if (m_hSession != NULL) {
        InternetCloseHandle(m_hSession);
        _atlInetSessionMap.RemoveKey(m_hSession);
        m_hSession = NULL;
    }
}

inline CHttpConnection* CInternetSession::GetHttpConnection(LPCTSTR pstrServer,
                                                            INTERNET_PORT nPort /* = INTERNET_INVALID_PORT_NUMBER */,
                                                            LPCTSTR pstrUserName /* = NULL */, LPCTSTR pstrPassword /* = NULL */)
{
    WTLASSERT(ATL::AtlIsValidString(pstrServer));

    CHttpConnection* pResult = new CHttpConnection(this, pstrServer, nPort, pstrUserName, pstrPassword, m_dwContext);
    return pResult;
}

inline CHttpConnection* CInternetSession::GetHttpConnection(LPCTSTR pstrServer, DWORD dwFlags,
                                                            INTERNET_PORT nPort /* = INTERNET_INVALID_PORT_NUMBER */,
                                                            LPCTSTR pstrUserName /* = NULL */, LPCTSTR pstrPassword /* = NULL */)
{
    WTLASSERT(ATL::AtlIsValidString(pstrServer));
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);

    CHttpConnection* pResult = new CHttpConnection(this, pstrServer, dwFlags, nPort, pstrUserName, pstrPassword, m_dwContext);
    return pResult;
}

inline CInternetFile* CInternetSession::OpenURL(LPCTSTR pstrURL, DWORD_PTR dwContext /* = 0 */,
                                                DWORD dwFlags /* = INTERNET_FLAG_TRANSFER_BINARY */, LPCTSTR pstrHeaders /* = NULL */,
                                                DWORD dwHeadersLength /* = 0 */)
{
    WTLASSERT(ATL::AtlIsValidString(pstrURL));
    WTLASSERT(dwHeadersLength == 0 || pstrHeaders != NULL);
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);

    // must have TRANSFER_BINARY or TRANSFER_ASCII but not both
#define _ATL_TRANSFER_MASK (INTERNET_FLAG_TRANSFER_BINARY | INTERNET_FLAG_TRANSFER_ASCII)
    WTLASSERT((dwFlags & _ATL_TRANSFER_MASK) != 0);
    WTLASSERT((dwFlags & _ATL_TRANSFER_MASK) != _ATL_TRANSFER_MASK);
#undef _ATL_TRANSFER_MASK

    if (dwContext == 1)
        dwContext = m_dwContext;

    DWORD dwServiceType;
    ATL::CString strServer;
    ATL::CString strObject;
    INTERNET_PORT nPort;
    CInternetFile* pResult;

    BOOL bParsed = AtlParseURL(pstrURL, dwServiceType, strServer, strObject, nPort);

    // if it turns out to be a file...
    if (bParsed && dwServiceType == WTL_INET_SERVICE_FILE) {
        ATLENSURE(INTERNET_MAX_URL_LENGTH >= strObject.GetLength());

        ATLTRACE(atlTraceInternet, 0, "Error: File service unimplemented: %8.8X\n", dwServiceType);
        pResult = NULL;
    }
    else {
        HINTERNET hOpener;

        hOpener = InternetOpenUrl(m_hSession, pstrURL, pstrHeaders, dwHeadersLength, dwFlags, dwContext);

        if (hOpener == NULL)
            AtlThrowInternetException(m_dwContext);

        if (!bParsed)
            dwServiceType = AtlGetInternetHandleType(hOpener);

        switch (dwServiceType) {
        case INTERNET_HANDLE_TYPE_GOPHER_FILE:
        case WTL_INET_SERVICE_GOPHER:
            ATLTRACE(atlTraceInternet, 0, "Error: Gopher service unimplemented: %8.8X\n", dwServiceType);
            pResult = NULL;
            break;

        case INTERNET_HANDLE_TYPE_FTP_FILE:
        case WTL_INET_SERVICE_FTP:
            pResult = new CInternetFile(hOpener, m_hSession, strObject, strServer, dwContext, TRUE);
            _atlInetSessionMap.SetAt(hOpener, this);
            break;

        case INTERNET_HANDLE_TYPE_HTTP_REQUEST:
        case WTL_INET_SERVICE_HTTP:
        case WTL_INET_SERVICE_HTTPS:
            pResult =
                new CHttpFile(hOpener, m_hSession, strObject, strServer, _atlHtmlVerbs[CHttpConnection::HTTP_VERB_GET], dwContext);
            _atlInetSessionMap.SetAt(hOpener, this);
            break;

        default:
            ATLTRACE(atlTraceInternet, 0, "Error: Unidentified service type: %8.8X\n", dwServiceType);
            pResult = NULL;
        }
    }

    return pResult;
}

inline BOOL CInternetSession::SetOption(DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength, DWORD dwFlags /* = 0 */)
{
    WTLASSERT(ATL::AtlIsValidAddress(lpBuffer, dwBufferLength, FALSE));
    WTLASSERT(dwOption >= INTERNET_FIRST_OPTION && dwOption <= INTERNET_LAST_OPTION);
    WTLASSERT(dwBufferLength != 0);
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);

    // bogus flag?
    WTLASSERT(dwFlags == 0 || ((dwFlags & ISO_VALID_FLAGS) == dwFlags));

    return __internetSetOptionEx(m_hSession, dwOption, lpBuffer, dwBufferLength, dwFlags);
}

inline BOOL CInternetSession::QueryOption(DWORD dwOption, LPVOID lpBuffer, LPDWORD lpdwBufferLength) const
{
    WTLASSERT(dwOption >= INTERNET_FIRST_OPTION && dwOption <= INTERNET_LAST_OPTION);
    WTLASSERT((lpdwBufferLength != NULL) && ATL::AtlIsValidAddress(lpdwBufferLength, sizeof(DWORD), FALSE));
    WTLASSERT(ATL::AtlIsValidAddress(lpBuffer, *lpdwBufferLength));
    WTLASSERT(*lpdwBufferLength != 0);

    return InternetQueryOption(m_hSession, dwOption, lpBuffer, lpdwBufferLength);
}

inline BOOL CInternetSession::QueryOption(DWORD dwOption, DWORD& dwValue) const
{
    DWORD dwLen = sizeof(DWORD);
    return InternetQueryOption(m_hSession, dwOption, &dwValue, &dwLen);
}

inline void CInternetSession::OnStatusCallback(DWORD_PTR dwContext, DWORD dwInternetStatus, LPVOID lpvStatusInformation,
                                               DWORD dwStatusInformationLength)
{
    WTLASSERT(m_bCallbackEnabled != NULL);

    if (m_pOldCallback != NULL) {
        (*m_pOldCallback)(m_hSession, dwContext, dwInternetStatus, lpvStatusInformation, dwStatusInformationLength);
    }
}

inline BOOL CInternetSession::EnableStatusCallback(BOOL bEnable /* = TRUE */)
{
    WTLASSERT(!bEnable || m_hSession != NULL);
    if (m_hSession == NULL)
        return FALSE;

    BOOL bResult = TRUE;

    if (bEnable) {
        WTLASSERT(!m_bCallbackEnabled);
        if (!m_bCallbackEnabled) {
            INTERNET_STATUS_CALLBACK pRet = InternetSetStatusCallback(m_hSession, AtlInternetStatusCallback);

            if (pRet != INTERNET_INVALID_STATUS_CALLBACK) {
                m_pOldCallback = pRet;
                m_bCallbackEnabled = TRUE;
            }
            else
                AtlThrowInternetException(m_dwContext);
        }
    }
    else {
        WTLASSERT(m_bCallbackEnabled);

        if (m_bCallbackEnabled) {
            InternetSetStatusCallback(m_hSession, NULL);
            m_bCallbackEnabled = FALSE;
        }
    }

    return bResult;
}

inline BOOL CInternetSession::SetCookie(LPCTSTR pstrUrl, LPCTSTR pstrCookieName, LPCTSTR pstrCookieData)
{
    WTLASSERT(ATL::AtlIsValidString(pstrUrl));
    WTLASSERT(ATL::AtlIsValidString(pstrCookieName));
    return InternetSetCookie(pstrUrl, pstrCookieName, pstrCookieData);
}

inline BOOL CInternetSession::GetCookie(_In_z_ LPCTSTR pstrUrl, _In_z_ LPCTSTR pstrCookieName,
                                        _Out_writes_z_(dwBufLen) LPTSTR pstrCookieData, _In_ DWORD dwBufLen)
{
    WTLASSERT(ATL::AtlIsValidString(pstrUrl));
    WTLASSERT(ATL::AtlIsValidString(pstrCookieName));
    WTLASSERT(pstrCookieData != NULL);
    return InternetGetCookie(pstrUrl, pstrCookieName, pstrCookieData, &dwBufLen);
}

inline DWORD CInternetSession::GetCookieLength(LPCTSTR pstrUrl, LPCTSTR pstrCookieName)
{
    WTLASSERT(ATL::AtlIsValidString(pstrUrl));
    WTLASSERT(ATL::AtlIsValidString(pstrCookieName));

    DWORD dwRet;
    if (!InternetGetCookie(pstrUrl, pstrCookieName, NULL, &dwRet))
        dwRet = 0;
    return dwRet;
}

inline BOOL CInternetSession::GetCookie(LPCTSTR pstrUrl, LPCTSTR pstrCookieName, ATL::CString& strCookieData)
{
    WTLASSERT(ATL::AtlIsValidString(pstrUrl));
    WTLASSERT(ATL::AtlIsValidString(pstrCookieName));

    DWORD dwLen = GetCookieLength(pstrUrl, pstrCookieName);

    LPTSTR pstrTarget = strCookieData.GetBuffer(dwLen + 1);
    BOOL bRetVal = InternetGetCookie(pstrUrl, pstrCookieName, pstrTarget, &dwLen);
    strCookieData.ReleaseBuffer(dwLen);

    if (!bRetVal)
        strCookieData.Empty();
    return bRetVal;
}

inline CInternetFile::CInternetFile(HINTERNET hFile, HINTERNET /* hSession */, LPCTSTR pstrFileName, LPCTSTR pstrServer,
                                    DWORD_PTR dwContext, BOOL bReadMode)
    : m_dwContext(dwContext)
{
    // caller must set _atlInetSessionMap()!

    WTLASSERT(ATL::AtlIsValidString(pstrServer));
    WTLASSERT(ATL::AtlIsValidString(pstrFileName));
    WTLASSERT(hFile != NULL);

    m_strFileName = pstrFileName;
    m_strServerName = pstrServer;
    m_hFile = hFile;
    m_bReadMode = bReadMode;

    m_pbReadBuffer = NULL;
    m_pbWriteBuffer = NULL;

    m_nReadBufferSize = 0;
    m_nReadBufferPos = 0;
    m_nWriteBufferSize = 0;
    m_nWriteBufferPos = 0;
    m_nReadBufferBytes = 0;
}

inline CInternetFile::CInternetFile(HINTERNET hFile, LPCTSTR pstrFileName, CInternetConnection* pConnection, BOOL bReadMode)
{
    WTLASSERT(ATL::AtlIsValidString(pstrFileName));
    WTLASSERT(pConnection != NULL);
    WTLASSERT(hFile != NULL);

    _atlInetSessionMap.SetAt(hFile, pConnection->GetSession());

    m_strFileName = pstrFileName;

    m_dwContext = pConnection->GetContext();
    m_strServerName = pConnection->GetServerName();
    m_hFile = hFile;
    m_bReadMode = bReadMode;

    m_pbReadBuffer = NULL;
    m_pbWriteBuffer = NULL;

    m_nReadBufferSize = 0;
    m_nReadBufferPos = 0;
    m_nWriteBufferSize = 0;
    m_nWriteBufferPos = 0;
    m_nReadBufferBytes = 0;
}

inline BOOL CInternetFile::QueryOption(DWORD dwOption, LPVOID lpBuffer, LPDWORD lpdwBufferLength) const
{
    WTLASSERT(dwOption >= INTERNET_FIRST_OPTION && dwOption <= INTERNET_LAST_OPTION);
    WTLASSERT((lpdwBufferLength != NULL) && ATL::AtlIsValidAddress(lpdwBufferLength, sizeof(DWORD), FALSE));
    WTLASSERT(ATL::AtlIsValidAddress(lpBuffer, *lpdwBufferLength));
    WTLASSERT(*lpdwBufferLength != 0);
    WTLASSERT(m_hFile != NULL);

    return InternetQueryOption(m_hFile, dwOption, lpBuffer, lpdwBufferLength);
}

inline BOOL CInternetFile::QueryOption(DWORD dwOption, DWORD& dwValue) const
{
    WTLASSERT(m_hFile != NULL);

    DWORD dwLen = sizeof(DWORD);
    return InternetQueryOption(m_hFile, dwOption, &dwValue, &dwLen);
}

inline BOOL CInternetFile::SetOption(DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength, DWORD dwFlags /* = 0 */)
{
    WTLASSERT(dwOption >= INTERNET_FIRST_OPTION && dwOption <= INTERNET_LAST_OPTION);
    WTLASSERT(ATL::AtlIsValidAddress(lpBuffer, dwBufferLength, FALSE));
    WTLASSERT(dwBufferLength != 0);
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);

    // bogus flag?
    WTLASSERT(dwFlags == 0 || ((dwFlags & ISO_VALID_FLAGS) == dwFlags));

    return __internetSetOptionEx(m_hFile, dwOption, lpBuffer, dwBufferLength, dwFlags);
}

inline BOOL CInternetFile::SetReadBufferSize(UINT nReadSize)
{
    BOOL bRet = TRUE;

    if (nReadSize != -1 && nReadSize != m_nReadBufferSize) {
        if (m_nReadBufferPos > nReadSize)
            bRet = FALSE;
        else {
            if (nReadSize == 0) {
                delete[] m_pbReadBuffer;
                m_pbReadBuffer = NULL;
            }
            else if (m_pbReadBuffer == NULL) {
                m_pbReadBuffer = new BYTE[nReadSize];
                m_nReadBufferPos = nReadSize;
            }
            else {
                DWORD dwMoved = m_nReadBufferSize - m_nReadBufferPos;
                LPBYTE pbTemp = m_pbReadBuffer;
                m_pbReadBuffer = new BYTE[nReadSize];

                if (dwMoved > 0 && dwMoved <= nReadSize) {
                    Checked::memcpy_s(m_pbReadBuffer, nReadSize, pbTemp + m_nReadBufferPos, dwMoved);

                    m_nReadBufferPos = 0;
                    m_nReadBufferBytes = dwMoved;
                }
                else {
                    m_nReadBufferBytes = 0;
                    m_nReadBufferPos = nReadSize;
                }
                delete[] pbTemp;
            }

            m_nReadBufferSize = nReadSize;
        }
    }

    return bRet;
}

inline BOOL CInternetFile::SetWriteBufferSize(UINT nWriteSize)
{
    BOOL bRet = TRUE;

    if (nWriteSize != m_nWriteBufferSize) {
        if (m_nWriteBufferPos > nWriteSize)
            Flush();

        if (nWriteSize == 0) {
            delete[] m_pbWriteBuffer;
            m_pbWriteBuffer = NULL;
        }
        else if (m_pbWriteBuffer == NULL) {
            m_pbWriteBuffer = new BYTE[nWriteSize];
            m_nWriteBufferPos = 0;
        }
        else {
            LPBYTE pbTemp = m_pbWriteBuffer;
            m_pbWriteBuffer = new BYTE[nWriteSize];
            if (m_nWriteBufferPos <= nWriteSize) {
                Checked::memcpy_s(m_pbWriteBuffer, nWriteSize, pbTemp, m_nWriteBufferPos);
            }
            delete[] pbTemp;
        }

        m_nWriteBufferSize = nWriteSize;
    }

    return bRet;
}

inline ULONGLONG CInternetFile::Seek(LONGLONG lOffset, UINT nFrom)
{
    WTLASSERT(m_hFile != NULL);
    WTLASSERT(m_bReadMode);
    WTLASSERT(m_pbReadBuffer == NULL);

    // can't do this on a file for writing
    // can't do this on a file that's got a buffer

    if (!m_bReadMode || m_pbReadBuffer != NULL)
        AtlThrowInternetException(m_dwContext, ERROR_INVALID_HANDLE);

    switch (nFrom) {
    case FILE_BEGIN:
    case FILE_CURRENT:
    case FILE_END:
        break;

    default:
        WTLASSERT(FALSE); // got a bogus nFrom value
        AtlThrowInternetException(m_dwContext, ERROR_INVALID_PARAMETER);
        break;
    }

    if ((lOffset < LONG_MIN) || (lOffset > LONG_MAX)) {
        AtlThrowInternetException(m_dwContext, ERROR_INVALID_PARAMETER);
    }

    LONG lRet;
    lRet = InternetSetFilePointer(m_hFile, LONG(lOffset), NULL, nFrom, m_dwContext);
    if (lRet == -1)
        AtlThrowInternetException(m_dwContext);

    return lRet;
}

inline CInternetFile::~CInternetFile()
{
    if (m_hFile != NULL) {
#ifdef _DEBUG
        ATLTRACE(atlTraceInternet, 0, _T("Warning: destroying an open %Ts with handle %8.8X\n"), _T("CInternetFile"), m_hFile);
#endif
        Close();
    }

    if (m_pbReadBuffer != NULL)
        delete m_pbReadBuffer;

    if (m_pbWriteBuffer != NULL)
        delete m_pbWriteBuffer;
}

inline void CInternetFile::Abort()
{
    if (m_hFile != NULL)
        Close();
    m_strFileName.Empty();
}

inline void CInternetFile::Flush()
{
    if (m_pbWriteBuffer != NULL && m_nWriteBufferPos > 0) {
        DWORD dwBytes;

        if (!InternetWriteFile(m_hFile, m_pbWriteBuffer, m_nWriteBufferPos, &dwBytes))
            AtlThrowInternetException(m_dwContext);

        if (dwBytes != m_nWriteBufferPos)
            AtlThrowInternetException(m_dwContext);

        m_nWriteBufferPos = 0;
    }
}

inline void CInternetFile::Close()
{
    if (m_hFile != NULL) {
        Flush();
        InternetCloseHandle(m_hFile);
        _atlInetSessionMap.RemoveKey(m_hFile);
        m_hFile = NULL;

        if (m_pbWriteBuffer != NULL) {
            delete[] m_pbWriteBuffer;
            m_pbWriteBuffer = NULL;
        }

        if (m_pbReadBuffer != NULL) {
            delete[] m_pbReadBuffer;
            m_pbReadBuffer = NULL;
        }
    }
}

inline UINT CInternetFile::Read(void* lpBuf, UINT nCount)
{
    WTLASSERT(ATL::AtlIsValidAddress(lpBuf, nCount));
    WTLASSERT(m_hFile != NULL);
    WTLASSERT(m_bReadMode);

    DWORD dwBytes;

    if (!m_bReadMode || m_hFile == NULL)
        AtlThrowInternetException(m_dwContext, ERROR_INVALID_HANDLE);

    if (m_pbReadBuffer == NULL) {
        if (!InternetReadFile(m_hFile, (LPVOID)lpBuf, nCount, &dwBytes))
            AtlThrowInternetException(m_dwContext);
        return dwBytes;
    }

    LPBYTE lpbBuf = (LPBYTE)lpBuf;

    // if the requested size is bigger than our buffer,
    // then handle it directly

    if (nCount >= m_nReadBufferSize) {
        DWORD dwMoved = max(0, (long)m_nReadBufferBytes - (long)m_nReadBufferPos);
        if (dwMoved <= nCount) {
            Checked::memcpy_s(lpBuf, nCount, m_pbReadBuffer + m_nReadBufferPos, dwMoved);
        }
        else {
            return 0; // output buffer not big enough.
        }

        m_nReadBufferPos = m_nReadBufferSize;
        if (!InternetReadFile(m_hFile, lpbBuf + dwMoved, nCount - dwMoved, &dwBytes))
            AtlThrowInternetException(m_dwContext);
        dwBytes += dwMoved;
    }
    else {
        if (m_nReadBufferPos + nCount >= m_nReadBufferBytes) {
            DWORD dwMoved = max(0, (long)m_nReadBufferBytes - (long)m_nReadBufferPos);
            if (dwMoved <= nCount) {
                Checked::memcpy_s(lpbBuf, nCount, m_pbReadBuffer + m_nReadBufferPos, dwMoved);
            }
            else {
                return 0;
            }

            DWORD dwRead;
            if (!InternetReadFile(m_hFile, m_pbReadBuffer, m_nReadBufferSize, &dwRead))
                AtlThrowInternetException(m_dwContext);
            m_nReadBufferBytes = dwRead;

            dwRead = min(nCount - dwMoved, m_nReadBufferBytes);
            Checked::memcpy_s(lpbBuf + dwMoved, nCount - dwMoved, m_pbReadBuffer, dwRead);
            m_nReadBufferPos = dwRead;
            dwBytes = dwMoved + dwRead;
        }
        else {
            Checked::memcpy_s(lpbBuf, nCount, m_pbReadBuffer + m_nReadBufferPos, nCount);
            m_nReadBufferPos += nCount;
            dwBytes = nCount;
        }
    }

    return dwBytes;
}

inline void CInternetFile::Write(const void* lpBuf, UINT nCount)
{
    WTLASSERT(m_hFile != NULL);
    WTLASSERT(ATL::AtlIsValidAddress(lpBuf, nCount, FALSE));
    WTLASSERT(m_bReadMode == FALSE || m_bReadMode == -1);

    if (m_bReadMode == TRUE || m_hFile == NULL)
        AtlThrowInternetException(m_dwContext, ERROR_INVALID_HANDLE);

    DWORD dwBytes;
    if (m_pbWriteBuffer == NULL) {
        if (!InternetWriteFile(m_hFile, lpBuf, nCount, &dwBytes))
            AtlThrowInternetException(m_dwContext);

        if (dwBytes != nCount)
            AtlThrowInternetException(m_dwContext);
    }
    else {
        if ((m_nWriteBufferPos + nCount) >= m_nWriteBufferSize) {
            // write what is in the buffer just now

            if (!InternetWriteFile(m_hFile, m_pbWriteBuffer, m_nWriteBufferPos, &dwBytes))
                AtlThrowInternetException(m_dwContext);

            // reset the buffer position since it is now clean

            m_nWriteBufferPos = 0;
        }

        // if we can't hope to buffer the write request,
        // do it immediately ... otherwise, buffer it!

        if (nCount >= m_nWriteBufferSize) {
            if (!InternetWriteFile(m_hFile, (LPVOID)lpBuf, nCount, &dwBytes))
                AtlThrowInternetException(m_dwContext);
        }
        else {
            if (m_nWriteBufferPos + nCount <= m_nWriteBufferSize) {
                Checked::memcpy_s(m_pbWriteBuffer + m_nWriteBufferPos, m_nWriteBufferSize - m_nWriteBufferPos, lpBuf, nCount);
                m_nWriteBufferPos += nCount;
            }
        }
    }
}

inline void CInternetFile::WriteString(LPCTSTR pstr)
{
    WTLASSERT(m_bReadMode == FALSE || m_bReadMode == -1);
    WTLASSERT(ATL::AtlIsValidString(pstr));
    WTLASSERT(m_hFile != NULL);

    if (m_bReadMode == TRUE)
        AtlThrowInternetException(m_dwContext, ERROR_INVALID_HANDLE);

    Write(pstr, static_cast<UINT>(AtlStrLen(pstr)) * sizeof(TCHAR));
}

inline LPTSTR CInternetFile::ReadString(_Out_writes_z_(nMax) LPTSTR pstr, _In_ UINT nMax)
{
    WTLASSERT(m_hFile != NULL);
    WTLASSERT(ATL::AtlIsValidAddress(pstr, nMax * sizeof(TCHAR)));
    DWORD dwRead;

    // if we're reading line-by-line, we must have a buffer

    if (m_pbReadBuffer == NULL) {
        if (!SetReadBufferSize(4096)) // arbitrary but reasonable
            return NULL;
        if (!InternetReadFile(m_hFile, m_pbReadBuffer, m_nReadBufferSize, &dwRead))
            AtlThrowInternetException(m_dwContext);
        m_nReadBufferBytes = dwRead;
        m_nReadBufferPos = 0;
    }

    LPSTR pstrChar = (LPSTR)(m_pbReadBuffer + m_nReadBufferPos);
    LPSTR pstrTarget = (LPSTR)pstr;

    UINT nMaxChars = (nMax - 1) * sizeof(TCHAR);
    while (nMaxChars) {
        if (m_nReadBufferPos >= m_nReadBufferBytes) {
            if (!InternetReadFile(m_hFile, m_pbReadBuffer, m_nReadBufferSize, &dwRead))
                AtlThrowInternetException(m_dwContext);
            m_nReadBufferBytes = dwRead;
            if (m_nReadBufferBytes == 0) {
                memset(pstrTarget, 0, (nMaxChars & (sizeof(TCHAR) - 1)) + sizeof(TCHAR));
                if (pstrTarget == (LPCSTR)pstr)
                    return NULL;
                else
                    return pstr;
            }
            else {
                m_nReadBufferPos = 0;
                pstrChar = (LPSTR)m_pbReadBuffer;
            }
        }

        if (*pstrChar != '\r') {
            *pstrTarget++ = *pstrChar;
            nMaxChars--;
        }

        m_nReadBufferPos++;
        if (*pstrChar++ == '\n')
            break;
    }

    memset(pstrTarget, 0, (nMaxChars & (sizeof(TCHAR) - 1)) + sizeof(TCHAR));
    return pstr;
}

inline BOOL CInternetFile::ReadString(ATL::CString& rString)
{
    WTLASSERT(m_hFile != NULL);

    rString = _T(""); // empty string without deallocating
    const int nMaxSize = 128;

    LPTSTR pstrPlace = rString.GetBuffer(nMaxSize);
    LPTSTR pstrResult;

    int nLen;

    do {
        pstrResult = ReadString(pstrPlace, nMaxSize);
        rString.ReleaseBuffer();

        // if string is read completely or EOF
        if (pstrResult == NULL || (nLen = AtlStrLen(pstrPlace)) < (nMaxSize - 1) || pstrPlace[nLen - 1] == '\n')
            break;

        nLen = rString.GetLength();
        pstrPlace = rString.GetBuffer(nMaxSize + nLen) + nLen;
    } while (1);

    // remove '\n' from end of string if present
    pstrPlace = rString.GetBuffer(0);
    nLen = rString.GetLength();
    if (nLen != 0 && pstrPlace[nLen - 1] == '\n')
        pstrPlace[nLen - 1] = '\0';
    rString.ReleaseBuffer();

    return ((pstrResult != NULL) || (nLen != 0));
}

inline ULONGLONG CInternetFile::GetLength() const
{
    WTLASSERT(m_hFile != NULL);

    DWORD dwRet = 0;

    if (m_hFile != NULL) {
        if (!InternetQueryDataAvailable(m_hFile, &dwRet, 0, 0))
            dwRet = 0;
    }

    return dwRet;
}

inline void CInternetFile::LockRange(ULONGLONG /* dwPos */, ULONGLONG /* dwCount */)
{
    WTLASSERT(m_hFile != NULL);

    ATLENSURE(false);
}

inline void CInternetFile::UnlockRange(ULONGLONG /* dwPos */, ULONGLONG /* dwCount */)
{
    WTLASSERT(m_hFile != NULL);

    ATLENSURE(false);
}

inline void CInternetFile::SetLength(ULONGLONG)
{
    WTLASSERT(m_hFile != NULL);

    ATLENSURE(false);
}

inline CInternetFile* CInternetFile::Duplicate() const
{
    ATLENSURE(false);
}

inline CInternetConnection::CInternetConnection(CInternetSession* pSession, LPCTSTR pstrServerName,
                                                INTERNET_PORT nPort /* = INTERNET_INVALID_PORT_NUMBER */,
                                                DWORD_PTR dwContext /* = 1 */)
    : m_strServerName(pstrServerName)
{
    WTLASSERT(pSession != NULL);
    WTLASSERT(pstrServerName != NULL);

    m_nPort = nPort;
    m_pSession = pSession;
    m_hConnection = NULL;
    if (dwContext == 1)
        dwContext = pSession->GetContext();
    m_dwContext = dwContext;
}

inline CInternetConnection::~CInternetConnection()
{
    if (m_hConnection != NULL) {
#ifdef _DEBUG
        ATLTRACE(atlTraceInternet, 0, _T("Warning: Disconnecting %Ts handle %8.8X in context %8.8X at destruction.\n"),
                 _T("CInternetConnection"), m_hConnection, m_dwContext);
#endif
        Close();
    }
}

inline BOOL CInternetConnection::SetOption(DWORD dwOption, LPVOID lpBuffer, DWORD dwBufferLength, DWORD dwFlags /* = 0 */)
{
    WTLASSERT(dwOption >= INTERNET_FIRST_OPTION && dwOption <= INTERNET_LAST_OPTION);
    WTLASSERT(ATL::AtlIsValidAddress(lpBuffer, dwBufferLength, FALSE));
    WTLASSERT(dwBufferLength != 0);
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);

    // bogus flag?
    WTLASSERT(dwFlags == 0 || ((dwFlags & ISO_VALID_FLAGS) == dwFlags));

    return __internetSetOptionEx(m_hConnection, dwOption, lpBuffer, dwBufferLength, dwFlags);
}

inline BOOL CInternetConnection::QueryOption(DWORD dwOption, LPVOID lpBuffer, LPDWORD lpdwBufferLength) const
{
    WTLASSERT(dwOption >= INTERNET_FIRST_OPTION && dwOption <= INTERNET_LAST_OPTION);
    WTLASSERT((lpdwBufferLength != NULL) && ATL::AtlIsValidAddress(lpdwBufferLength, sizeof(DWORD), FALSE));
    WTLASSERT(ATL::AtlIsValidAddress(lpBuffer, *lpdwBufferLength));
    WTLASSERT(*lpdwBufferLength != 0);

    return InternetQueryOption(m_hConnection, dwOption, lpBuffer, lpdwBufferLength);
}

inline BOOL CInternetConnection::QueryOption(DWORD dwOption, DWORD& dwValue) const
{
    DWORD dwLen = sizeof(DWORD);
    return InternetQueryOption(m_hConnection, dwOption, &dwValue, &dwLen);
}

inline void CInternetConnection::Close()
{
    if (m_hConnection != NULL) {
        InternetCloseHandle(m_hConnection);
        _atlInetSessionMap.RemoveKey(m_hConnection);
        m_hConnection = NULL;
    }
}

inline CHttpConnection::~CHttpConnection() {}

inline CHttpConnection::CHttpConnection(CInternetSession* pSession, HINTERNET hConnected, LPCTSTR pstrServer,
                                        DWORD_PTR dwContext /* = 0 */)
    : CInternetConnection(pSession, pstrServer, INTERNET_INVALID_PORT_NUMBER, dwContext)
{
    WTLASSERT(pSession != NULL);
    WTLASSERT(ATL::AtlIsValidString(pstrServer));

    m_strServerName = pstrServer;

    BOOL bBadType = FALSE;
    if (AtlGetInternetHandleType(hConnected) != INTERNET_HANDLE_TYPE_CONNECT_HTTP) {
        WTLASSERT(FALSE); // used the wrong handle type
        bBadType = TRUE;
    }

    m_hConnection = hConnected;
    if (m_hConnection == NULL || bBadType)
        AtlThrowInternetException(m_dwContext, ERROR_INVALID_HANDLE);
    else
        _atlInetSessionMap.SetAt(m_hConnection, m_pSession);
}

inline CHttpConnection::CHttpConnection(CInternetSession* pSession, LPCTSTR pstrServer,
                                        INTERNET_PORT nPort /* = INTERNET_INVALID_PORT_NUMBER */, LPCTSTR pstrUserName /* = NULL */,
                                        LPCTSTR pstrPassword /* = NULL */, DWORD_PTR dwContext /* = 1 */)
    : CInternetConnection(pSession, pstrServer, nPort, dwContext)
{
    WTLASSERT(pSession != NULL);
    WTLASSERT(ATL::AtlIsValidString(pstrServer));

    m_strServerName = pstrServer;

    m_hConnection =
        InternetConnect((HINTERNET)*pSession, pstrServer, nPort, pstrUserName, pstrPassword, INTERNET_SERVICE_HTTP, 0, m_dwContext);

    if (m_hConnection == NULL)
        AtlThrowInternetException(m_dwContext);
    else
        _atlInetSessionMap.SetAt(m_hConnection, m_pSession);
}

inline CHttpConnection::CHttpConnection(CInternetSession* pSession, LPCTSTR pstrServer, DWORD dwFlags,
                                        INTERNET_PORT nPort /* = INTERNET_INVALID_PORT_NUMBER */, LPCTSTR pstrUserName /* = NULL */,
                                        LPCTSTR pstrPassword /* = NULL */, DWORD_PTR dwContext /* = 1 */)
    : CInternetConnection(pSession, pstrServer, nPort, dwContext)
{
    WTLASSERT(pSession != NULL);
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);
    WTLASSERT(ATL::AtlIsValidString(pstrServer));

    m_strServerName = pstrServer;

    m_hConnection = InternetConnect((HINTERNET)*pSession, pstrServer, nPort, pstrUserName, pstrPassword, INTERNET_SERVICE_HTTP,
                                    dwFlags, m_dwContext);

    if (m_hConnection == NULL)
        AtlThrowInternetException(m_dwContext);
    else
        _atlInetSessionMap.SetAt(m_hConnection, m_pSession);
}

inline CHttpFile* CHttpConnection::OpenRequest(LPCTSTR pstrVerb, LPCTSTR pstrObjectName, LPCTSTR pstrReferer, DWORD_PTR dwContext,
                                               LPCTSTR* ppstrAcceptTypes, LPCTSTR pstrVersion, DWORD dwFlags)
{
    WTLASSERT(m_hConnection != NULL);
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);

    if (dwContext == 1)
        dwContext = m_dwContext;

    if (pstrVersion == NULL)
        pstrVersion = HTTP_VERSION;

    HINTERNET hFile;
    hFile = HttpOpenRequest(m_hConnection, pstrVerb, pstrObjectName, pstrVersion, pstrReferer, ppstrAcceptTypes, dwFlags, dwContext);

    CHttpFile* pRet = new CHttpFile(hFile, pstrVerb, pstrObjectName, this);
    if (pRet != NULL)
        pRet->m_dwContext = dwContext;
    return pRet;
}

inline CHttpFile* CHttpConnection::OpenRequest(int nVerb, LPCTSTR pstrObjectName, LPCTSTR pstrReferer /* = NULL */,
                                               DWORD_PTR dwContext, LPCTSTR* ppstrAcceptTypes /* = NULL */,
                                               LPCTSTR pstrVersion /* = NULL */, DWORD dwFlags)
{
    WTLASSERT(m_hConnection != NULL);
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);
    WTLASSERT(ATL::AtlIsValidString(pstrObjectName));

    WTLASSERT(nVerb >= _HTTP_VERB_MIN && nVerb <= _HTTP_VERB_MAX);

    LPCTSTR pstrVerb;
    if (nVerb >= _HTTP_VERB_MIN && nVerb <= _HTTP_VERB_MAX)
        pstrVerb = _atlHtmlVerbs[nVerb];
    else
        pstrVerb = _T("");

    return OpenRequest(pstrVerb, pstrObjectName, pstrReferer, dwContext, ppstrAcceptTypes, pstrVersion, dwFlags);
}

inline CHttpFile::CHttpFile(HINTERNET hFile, HINTERNET hSession, LPCTSTR pstrObject, LPCTSTR pstrServer, LPCTSTR pstrVerb,
                            DWORD_PTR dwContext)
    : CInternetFile(hFile, hSession, pstrObject, pstrServer, dwContext, TRUE)
    , m_strVerb(pstrVerb)
    , m_strObject(pstrObject)
{
    // caller must set _atlInetSessionMap!
    WTLASSERT(ATL::AtlIsValidString(pstrVerb));
    m_hConnection = hFile;
}

inline CHttpFile::CHttpFile(HINTERNET hFile, LPCTSTR pstrVerb, LPCTSTR pstrObject, CHttpConnection* pConnection)
    : CInternetFile(hFile, pstrObject, pConnection, TRUE)
    , m_strVerb(pstrVerb)
    , m_strObject(pstrObject)
{
    WTLASSERT(pstrVerb != NULL);
    WTLASSERT(pstrObject != NULL);
    WTLASSERT(pConnection != NULL);
    m_hConnection = hFile;
}

inline CHttpFile::~CHttpFile() {}

inline DWORD CHttpFile::ErrorDlg(HWND hParentWnd /* = NULL */, DWORD dwError /* = ERROR_INTERNET_INCORRECT_PASSWORD */,
                                 DWORD dwFlags /* = FLAGS_ERROR_UI_FLAGS_GENERATE_DATA | FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS*/,
                                 LPVOID* lppvData /* = NULL */)
{
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);
    HWND hWnd;
    LPVOID lpEmpty;
    LPVOID* lppvHolder;

    if (lppvData == NULL) {
        lpEmpty = NULL;
        lppvHolder = &lpEmpty;
    }
    else
        lppvHolder = lppvData;

    if (hParentWnd == NULL)
        hWnd = GetDesktopWindow();
    else
        hWnd = hParentWnd;

    return InternetErrorDlg(hWnd, m_hFile, dwError, dwFlags, lppvHolder);
}

inline ATL::CString CHttpFile::GetVerb() const
{
    WTLASSERT(m_hFile != NULL);

    return m_strVerb;
}

#pragma push_macro("GetObject")
#undef GetObject
inline ATL::CString CHttpFile::GetObject() const
{
    WTLASSERT(m_hFile != NULL);

    return m_strObject;
}
#pragma pop_macro("GetObject")

inline ATL::CString CHttpFile::GetFileURL() const
{
    WTLASSERT(m_hFile != NULL);

    ATL::CString str(_atlURLhttp);
    if (m_hConnection != NULL) {
        str += m_strServerName;
        INT_PTR nLen = m_strObject.GetLength();
        if (nLen > 0) {
            if (m_strObject[0] != '/' && m_strObject[0] != '\\')
                str += '/';
            str += m_strObject;
        }
    }

    return str;
}

inline BOOL CHttpFile::AddRequestHeaders(LPCTSTR pstrHeaders, DWORD dwModifiers /* = HTTP_ADDREQ_FLAG_ADD */,
                                         int dwHeadersLen /* = -1 */)
{
    WTLASSERT(ATL::AtlIsValidString(pstrHeaders));
    WTLASSERT(dwHeadersLen == 0 || pstrHeaders != NULL);
    WTLASSERT(m_hFile != NULL);

    if (dwHeadersLen == -1)
        if (pstrHeaders == NULL)
            dwHeadersLen = 0;
        else
            dwHeadersLen = static_cast<DWORD>(_tcslen(pstrHeaders));

    return HttpAddRequestHeaders(m_hFile, pstrHeaders, dwHeadersLen, dwModifiers);
}

inline BOOL CHttpFile::AddRequestHeaders(ATL::CString& str, DWORD dwModifiers /* = HTTP_ADDREQ_FLAG_ADD */)
{
    return AddRequestHeaders((LPCTSTR)str, dwModifiers, (int)str.GetLength());
}

inline BOOL CHttpFile::SendRequest(LPCTSTR pstrHeaders /* = NULL */, DWORD dwHeadersLen /* = 0 */, LPVOID lpOptional /* = NULL */,
                                   DWORD dwOptionalLen /* = 0 */)
{
    WTLASSERT(dwOptionalLen == 0 || lpOptional != NULL);
    WTLASSERT(dwHeadersLen == 0 || pstrHeaders != NULL);
    WTLASSERT(m_hFile != NULL);

    BOOL bRet = HttpSendRequest(m_hFile, pstrHeaders, dwHeadersLen, lpOptional, dwOptionalLen);

    if (!bRet)
        AtlThrowInternetException(m_dwContext);

    return bRet;
}

inline BOOL CHttpFile::EndRequest(DWORD dwFlags /* = 0 */, LPINTERNET_BUFFERS lpBuffIn /* = NULL */, DWORD_PTR dwContext /* = 1 */)
{
    WTLASSERT(m_hFile != NULL);
    WTLASSERT(m_bReadMode == -1);

    if (dwContext == 1)
        dwContext = m_dwContext;

    BOOL bRet = HttpEndRequest(m_hFile, lpBuffIn, dwFlags, dwContext);

    if (!bRet)
        AtlThrowInternetException(m_dwContext);
    return bRet;
}

inline BOOL CHttpFile::SendRequestEx(DWORD dwTotalLen, DWORD dwFlags /* = HSR_INITIATE */, DWORD_PTR dwContext /* = 1 */)
{
    WTLASSERT(m_hFile != NULL);

    INTERNET_BUFFERS buffer;
    memset(&buffer, 0, sizeof(buffer));
    buffer.dwStructSize = sizeof(buffer);
    buffer.dwBufferTotal = dwTotalLen;

    if (dwContext == 1)
        dwContext = m_dwContext;

    return SendRequestEx(&buffer, NULL, dwFlags, dwContext);
}

inline BOOL CHttpFile::SendRequestEx(LPINTERNET_BUFFERS lpBuffIn, LPINTERNET_BUFFERS lpBuffOut, DWORD dwFlags /* = HSR_INITIATE */,
                                     DWORD_PTR dwContext /* = 1 */)
{
    WTLASSERT(m_hFile != NULL);
    WTLASSERT((lpBuffIn == NULL) || ATL::AtlIsValidAddress(lpBuffIn, sizeof(INTERNET_BUFFERS), FALSE));
    WTLASSERT((lpBuffOut == NULL) || ATL::AtlIsValidAddress(lpBuffOut, sizeof(INTERNET_BUFFERS), FALSE));

    if (dwContext == 1)
        dwContext = m_dwContext;

    BOOL bRet = HttpSendRequestEx(m_hFile, lpBuffIn, lpBuffOut, dwFlags, dwContext);

    if (!bRet)
        AtlThrowInternetException(m_dwContext);

    m_bReadMode = -1;
    return bRet;
}

inline BOOL CHttpFile::SendRequest(ATL::CString& strHeaders, LPVOID lpOptional /* = NULL */, DWORD dwOptionalLen /* = 0 */)
{
    WTLASSERT(dwOptionalLen == 0 || lpOptional != NULL);
    WTLASSERT(m_hFile != NULL);

    return SendRequest((LPCTSTR)strHeaders, (ULONG)strHeaders.GetLength(), lpOptional, dwOptionalLen);
}

inline BOOL CHttpFile::QueryInfo(DWORD dwInfoLevel, LPVOID lpvBuffer, LPDWORD lpdwBufferLength, LPDWORD lpdwIndex) const
{
    WTLASSERT(lpvBuffer != NULL && *lpdwBufferLength > 0);
    WTLASSERT(m_hFile != NULL);

    return HttpQueryInfo(m_hFile, dwInfoLevel, lpvBuffer, lpdwBufferLength, lpdwIndex);
}

inline BOOL CHttpFile::QueryInfo(DWORD dwInfoLevel, DWORD& dwResult, LPDWORD lpdwIndex /* = NULL */) const
{
    dwInfoLevel |= HTTP_QUERY_FLAG_NUMBER;
    DWORD dwDWSize = sizeof(DWORD);
    return QueryInfo(dwInfoLevel, &dwResult, &dwDWSize, lpdwIndex);
}

inline BOOL CHttpFile::QueryInfo(DWORD dwInfoLevel, SYSTEMTIME* pSystemTime, LPDWORD lpdwIndex /* = NULL */) const
{
    dwInfoLevel |= HTTP_QUERY_FLAG_SYSTEMTIME;
    DWORD dwTimeSize = sizeof(SYSTEMTIME);
    return QueryInfo(dwInfoLevel, pSystemTime, &dwTimeSize, lpdwIndex);
}

inline BOOL CHttpFile::QueryInfoStatusCode(DWORD& dwStatusCode) const
{
    WTLASSERT(m_hFile != NULL);

    TCHAR szBuffer[80];
    DWORD dwLen = _countof(szBuffer);
    BOOL bRet;

    bRet = HttpQueryInfo(m_hFile, HTTP_QUERY_STATUS_CODE, szBuffer, &dwLen, NULL);

    if (bRet)
        dwStatusCode = (DWORD)_ttol(szBuffer);
    return bRet;
}

inline BOOL CHttpFile::QueryInfo(DWORD dwInfoLevel, ATL::CString& str, LPDWORD lpdwIndex) const
{
    WTLASSERT(dwInfoLevel <= HTTP_QUERY_MAX);
    WTLASSERT(m_hFile != NULL);

    BOOL bRet;
    DWORD dwLen = 0;

    // ask for nothing to see how long the return really is

    str.Empty();
    if (HttpQueryInfo(m_hFile, dwInfoLevel, NULL, &dwLen, 0))
        bRet = TRUE;
    else {
        // now that we know how long it is, ask for exactly that much
        // space and really request the header from the API

        LPTSTR pstr = str.GetBufferSetLength(dwLen / sizeof(TCHAR));
        bRet = HttpQueryInfo(m_hFile, dwInfoLevel, pstr, &dwLen, lpdwIndex);
        if (bRet)
            str.ReleaseBuffer(dwLen / sizeof(TCHAR));
        else
            str.ReleaseBuffer(0);
    }

    return bRet;
}

inline DWORD_PTR CInternetSession::GetContext() const
{
    return m_dwContext;
}

inline ATL::CString CInternetConnection::GetServerName() const
{
    return m_strServerName;
}

inline CInternetSession* CInternetConnection::GetSession() const
{
    return m_pSession;
}

inline CInternetSession::operator HINTERNET() const
{
    return m_hSession;
}

inline BOOL CInternetSession::SetOption(DWORD dwOption, DWORD dwValue, DWORD dwFlags /* = 0 */)
{
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);
    return SetOption(dwOption, &dwValue, sizeof(dwValue), dwFlags);
}

inline CInternetConnection::operator HINTERNET() const
{
    return m_hConnection;
}

inline DWORD_PTR CInternetConnection::GetContext() const
{
    return m_dwContext;
}

inline BOOL CInternetConnection::SetOption(DWORD dwOption, DWORD dwValue, DWORD dwFlags /* = 0 */)
{
    return SetOption(dwOption, &dwValue, sizeof(dwValue), dwFlags);
}

inline DWORD_PTR CInternetFile::GetContext() const
{
    return m_dwContext;
}

inline CInternetFile::operator HINTERNET() const
{
    return m_hFile;
}

inline BOOL CInternetFile::SetOption(DWORD dwOption, DWORD dwValue, DWORD dwFlags /* = 0 */)
{
    WTLASSERT((dwFlags & INTERNET_FLAG_ASYNC) == 0);
    return SetOption(dwOption, &dwValue, sizeof(dwValue), dwFlags);
}

inline void AtlThrowInternetException(DWORD_PTR dwContext, DWORD dwError /* = 0*/)
{
    if (dwError == 0)
        dwError = ::GetLastError();

    ATLTRACE(atlTraceInternet, 0, "Warning: throwing CInternetException for error %d\n", dwError);
    throw CInternetException(dwError, dwContext);
}

static inline BOOL _AtlParseURLWorker(LPCTSTR pstrURL, LPURL_COMPONENTS lpComponents, DWORD& dwServiceType, INTERNET_PORT& nPort,
                                      DWORD dwFlags)
{
    // this function will return bogus stuff if lpComponents
    // isn't set up to copy the components

    WTLASSERT(lpComponents != NULL && pstrURL != NULL);
    if (lpComponents == NULL || pstrURL == NULL)
        return FALSE;
    WTLASSERT(lpComponents->dwHostNameLength == 0 || lpComponents->lpszHostName != NULL);
    WTLASSERT(lpComponents->dwUrlPathLength == 0 || lpComponents->lpszUrlPath != NULL);
    WTLASSERT(lpComponents->dwUserNameLength == 0 || lpComponents->lpszUserName != NULL);
    WTLASSERT(lpComponents->dwPasswordLength == 0 || lpComponents->lpszPassword != NULL);

    WTLASSERT(ATL::AtlIsValidAddress(lpComponents, sizeof(URL_COMPONENTS), TRUE));

    LPTSTR pstrCanonicalizedURL;
    TCHAR szCanonicalizedURL[INTERNET_MAX_URL_LENGTH];
    DWORD dwNeededLength = INTERNET_MAX_URL_LENGTH;
    BOOL bRetVal;
    BOOL bMustFree = FALSE;

    // Decoding is done in InternetCrackUrl/UrlUnescape
    // so we don't need the ICU_DECODE flag here.

    DWORD dwCanonicalizeFlags = dwFlags & (ICU_NO_ENCODE | ICU_NO_META | ICU_ENCODE_SPACES_ONLY | ICU_BROWSER_MODE);

    DWORD dwCrackFlags = 0;

    BOOL bUnescape = FALSE;

    if ((dwFlags & (ICU_ESCAPE | ICU_DECODE)) && (lpComponents->dwUrlPathLength != 0)) {

        // We use only the ICU_ESCAPE flag for decoding even if
        // ICU_DECODE is passed.

        // Also, if ICU_BROWSER_MODE is passed we do the unescaping
        // manually because InternetCrackUrl doesn't do
        // Browser mode unescaping

        if (dwFlags & ICU_BROWSER_MODE)
            bUnescape = TRUE;
        else
            dwCrackFlags |= ICU_ESCAPE;
    }

    bRetVal = InternetCanonicalizeUrl(pstrURL, szCanonicalizedURL, &dwNeededLength, dwCanonicalizeFlags);

    if (!bRetVal) {
        if (::GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            return FALSE;

        pstrCanonicalizedURL = new TCHAR[dwNeededLength];
        if (pstrCanonicalizedURL == NULL)
            return FALSE;

        bMustFree = TRUE;
        bRetVal = InternetCanonicalizeUrl(pstrURL, pstrCanonicalizedURL, &dwNeededLength, dwCanonicalizeFlags);
        if (!bRetVal) {
            delete[] pstrCanonicalizedURL;
            return FALSE;
        }
    }
    else {
        pstrCanonicalizedURL = szCanonicalizedURL;
    }

    // now that it's safely canonicalized, crack it

    bRetVal = InternetCrackUrl(pstrCanonicalizedURL, 0, dwCrackFlags, lpComponents);

    if (bUnescape) {
        // Length of buffer passed to UrlUnescape cannot be larger than INTERNET_MAX_URL_LENGTH characters.
        if (AtlStrLen(lpComponents->lpszUrlPath) >= INTERNET_MAX_URL_LENGTH ||
            FAILED(UrlUnescape(lpComponents->lpszUrlPath, NULL, NULL, URL_UNESCAPE_INPLACE | URL_DONT_UNESCAPE_EXTRA_INFO))) {
            if (bMustFree)
                delete[] pstrCanonicalizedURL;

            return FALSE;
        }

        lpComponents->dwUrlPathLength = static_cast<DWORD>(AtlStrLen(lpComponents->lpszUrlPath));
    }

    if (bMustFree) {
        delete[] pstrCanonicalizedURL;
    }

    // convert to MFC-style service ID

    if (!bRetVal) {
        dwServiceType = WTL_INET_SERVICE_UNK;
    }
    else {
        nPort = lpComponents->nPort;
        switch (lpComponents->nScheme) {
        case INTERNET_SCHEME_FTP:
            dwServiceType = WTL_INET_SERVICE_FTP;
            break;

        case INTERNET_SCHEME_GOPHER:
            dwServiceType = WTL_INET_SERVICE_GOPHER;
            break;

        case INTERNET_SCHEME_HTTP:
            dwServiceType = WTL_INET_SERVICE_HTTP;
            break;

        case INTERNET_SCHEME_HTTPS:
            dwServiceType = WTL_INET_SERVICE_HTTPS;
            break;

        case INTERNET_SCHEME_FILE:
            dwServiceType = WTL_INET_SERVICE_FILE;
            break;

        case INTERNET_SCHEME_NEWS:
            dwServiceType = WTL_INET_SERVICE_NNTP;
            break;

        case INTERNET_SCHEME_MAILTO:
            dwServiceType = WTL_INET_SERVICE_MAILTO;
            break;

        default:
            dwServiceType = WTL_INET_SERVICE_UNK;
        }
    }

    return bRetVal;
}

inline BOOL AtlParseURL(LPCTSTR pstrURL, DWORD& dwServiceType, ATL::CString& strServer, ATL::CString& strObject, INTERNET_PORT& nPort)
{
    dwServiceType = WTL_INET_SERVICE_UNK;

    WTLASSERT(pstrURL != NULL);
    if (pstrURL == NULL)
        return FALSE;

    URL_COMPONENTS urlComponents;
    memset(&urlComponents, 0, sizeof(URL_COMPONENTS));
    urlComponents.dwStructSize = sizeof(URL_COMPONENTS);

    urlComponents.dwHostNameLength = INTERNET_MAX_URL_LENGTH;
    urlComponents.lpszHostName = strServer.GetBuffer(INTERNET_MAX_URL_LENGTH + 1);
    urlComponents.dwUrlPathLength = INTERNET_MAX_URL_LENGTH;
    urlComponents.lpszUrlPath = strObject.GetBuffer(INTERNET_MAX_URL_LENGTH + 1);

    BOOL bRetVal = _AtlParseURLWorker(pstrURL, &urlComponents, dwServiceType, nPort, ICU_BROWSER_MODE);

    strServer.ReleaseBuffer();
    strObject.ReleaseBuffer();
    return bRetVal;
}

inline DWORD AtlGetInternetHandleType(HINTERNET hQuery)
{
    DWORD dwServiceType;
    DWORD dwTypeLen = sizeof(dwServiceType);
    if (hQuery == NULL || !InternetQueryOption(hQuery, INTERNET_OPTION_HANDLE_TYPE, &dwServiceType, &dwTypeLen))
        return WTL_INET_SERVICE_UNK;
    else
        return dwServiceType;
}

} // namespace WTL

#endif // __WTL_INET_H__
