// Windows Template Library Extensions - wtlext version 1.0
// Copyright (C) FireDaemon Technologies Limited. All rights reserved.
//
// The use and distribution terms for this software are covered by the
// Microsoft Public License (http://opensource.org/licenses/MS-PL)
// which can be found in the file MS-PL.txt at the root folder.

#pragma once
#ifndef __WTL_REGKEYEX_H__
#define __WTL_REGKEYEX_H__

#include <atlbase.h>

#include "wtlextdef.h"

namespace WTL {

/// <summary>
/// Registry Key wrapper that utilizes RegGetValue()/RegSetKeyValue() and the ability to use paths for registry value names</summary>
class CRegKeyEx2 : public ATL::CRegKey {
public:
    /// <summary>
    /// CRegKeyEx2 constructor</summary>
    /// <param name="pTM">Pointer to CAtlTransactionManager object</param>
    CRegKeyEx2(_In_opt_ CAtlTransactionManager* pTM = NULL) throw()
        : CRegKey(pTM)
        , m_bWithSubKeyPaths(false)
    {
    }
#if _MSC_VER >= 1600
    CRegKeyEx2(CRegKeyEx2&& key) throw()
        : CRegKey(key)
        , m_bWithSubKeyPaths(key.m_bWithSubKeyPaths)
    {
    }
#else
    CRegKeyEx2(_Inout_ CRegKeyEx2& key) throw()
        : CRegKey(key)
        , m_bWithSubKeyPaths(key.m_bWithSubKeyPaths)
    {
    }
#endif
    /// <summary>
    /// CRegKeyEx2 constructor</summary>
    /// <param name="bWithSubKeyPaths">Enable parsing of paths into subkey and value name</param>
    explicit CRegKeyEx2(_In_ HKEY hKey, bool bWithSubKeyPaths = false) throw()
        : CRegKey(hKey)
        , m_bWithSubKeyPaths(bWithSubKeyPaths)
    {
    }

#if _MSC_VER >= 1600
    CRegKeyEx2& operator=(CRegKeyEx2&& key) throw()
    {
        CRegKey::operator=(key);
        m_bWithSubKeyPaths = key.m_bWithSubKeyPaths;
        return *this;
    }
#else
    CRegKeyEx2& operator=(_Inout_ CRegKeyEx2& key) throw()
    {
        CRegKey::operator=(key);
        m_bWithSubKeyPaths = key.m_bWithSubKeyPaths;
        return *this;
    }
#endif

    // Attributes
public:
    bool m_bWithSubKeyPaths;

    // Deprecated Operations
private:
    LSTATUS SetValue(_In_ DWORD dwValue, _In_opt_z_ PCTSTR lpszValueName);
    LSTATUS SetValue(_In_z_ PCTSTR lpszValue, _In_opt_z_ PCTSTR lpszValueName = NULL, _In_ bool bMulti = false,
                     _In_ int nValueSize = -1);

    // Operations
public:
    LSTATUS SetValue(_In_opt_z_ PCTSTR pszPath, _In_ DWORD dwType, _In_opt_ const void* pValue, _In_ ULONG nBytes) throw();
    LSTATUS SetGUIDValue(_In_opt_z_ PCTSTR pszPath, _In_ REFGUID guidValue) throw();
    LSTATUS SetBinaryValue(_In_opt_z_ PCTSTR pszPath, _In_opt_ const void* pValue, _In_ ULONG nBytes) throw();
    LSTATUS SetDWORDValue(_In_opt_z_ PCTSTR pszPath, _In_ DWORD dwValue) throw();
    LSTATUS SetQWORDValue(_In_opt_z_ PCTSTR pszPath, _In_ ULONGLONG qwValue) throw();
    LSTATUS SetStringValue(_In_opt_z_ PCTSTR pszPath, _In_opt_z_ PCTSTR pszValue, _In_ DWORD dwType = REG_SZ,
                           _In_ int nValueSize = -1) throw();
    LSTATUS SetMultiStringValue(_In_opt_z_ PCTSTR pszPath, _In_z_ PCTSTR pszValue) throw();

    LSTATUS QueryValue(_In_opt_z_ PCTSTR pszPath, _Out_opt_ DWORD* pdwType, _Out_opt_ void* pData, _Inout_ ULONG* pnBytes) throw();
    LSTATUS QueryGUIDValue(_In_opt_z_ PCTSTR pszPath, _Out_ GUID& guidValue) throw();
    LSTATUS QueryBinaryValue(_In_opt_z_ PCTSTR pszPath, _Out_opt_ void* pValue, _Inout_opt_ ULONG* pnBytes) throw();
    LSTATUS QueryDWORDValue(_In_opt_z_ PCTSTR pszSubKey, _Out_ DWORD& dwValue) throw();
    LSTATUS QueryQWORDValue(_In_opt_z_ PCTSTR pszPath, _Out_ ULONGLONG& qwValue) throw();
    LSTATUS QueryStringValue(_In_opt_z_ PCTSTR pszPath, _Out_writes_to_opt_(*pnChars, *pnChars) PTSTR pszValue,
                             _Inout_ ULONG* pnChars) throw();
    LSTATUS QueryMultiStringValue(_In_opt_z_ PCTSTR pszPath, _Out_writes_to_opt_(*pnChars, *pnChars) PTSTR pszValue,
                                  _Inout_ ULONG* pnChars) throw();
};

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#include <atlalloc.h>

#include "wtlextdef.h"

namespace WTL {

static inline LSTATUS WTLQueryKeyValueImpl(HKEY hKey, bool bWithSubKeyPaths, PCTSTR pszPath, DWORD dwFlags, DWORD* pdwType,
                                           PVOID pData, DWORD* pcbData)
{
    USES_ATL_SAFE_ALLOCA;
    _TCHAR* pszSubKey = NULL;
    const _TCHAR* pszValueName = pszPath;
    if (const _TCHAR* slashPos = bWithSubKeyPaths ? _tcsrchr(pszPath, _T('\\')) : NULL) {
        pszValueName = slashPos + 1;
        if (SIZE_T n = slashPos - pszPath) {
            pszSubKey = (PTSTR)_ATL_SAFE_ALLOCA((n + 1) * sizeof(_TCHAR), _ATL_SAFE_ALLOCA_DEF_THRESHOLD);
            if (!pszSubKey)
                return E_OUTOFMEMORY;
#ifdef _UNICODE
            wmemcpy(pszSubKey, pszPath, n);
#else
            memcpy(pszSubKey, pszPath, n);
#endif
            pszSubKey[n] = _T('\0');
        }
    }

#if (WTL_NTDDI_VERSION < NTDDI_VISTA)
    HKEY hkSub;
    if (LSTATUS lRes = RegOpenKeyEx(hKey, pszSubKey, 0, KEY_QUERY_VALUE, &hkSub))
        return lRes;

    DWORD dwType;
    if (!pdwType)
        pdwType = &dwType;
    DWORD cbBuf; // Save buffer byte size for appending 'null-terminating' characters further down
    if (pData)
        cbBuf = *pcbData;
    LSTATUS lRes = RegQueryValueEx(hkSub, pszValueName, NULL, pdwType, PBYTE(pData), pcbData);
    RegCloseKey(hkSub);
    hkSub = NULL;

    if (lRes != ERROR_SUCCESS)
        return lRes;

    // like `RegGetValue()`: check whether actual type matches requested type
    switch (*pdwType) {
    case REG_NONE:
        if (!(dwFlags & RRF_RT_REG_NONE))
            return ERROR_UNSUPPORTED_TYPE;
        break;
    case REG_DWORD:
        if (!(dwFlags & RRF_RT_DWORD))
            return ERROR_UNSUPPORTED_TYPE;
        break;
    case REG_QWORD:
        if (!(dwFlags & RRF_RT_QWORD))
            return ERROR_UNSUPPORTED_TYPE;
        break;
    case REG_SZ:
        if (!(dwFlags & RRF_RT_REG_SZ))
            return ERROR_UNSUPPORTED_TYPE;
        break;
    case REG_EXPAND_SZ:
        if (!(dwFlags & RRF_RT_REG_EXPAND_SZ))
            return ERROR_UNSUPPORTED_TYPE;
        break;
    case REG_MULTI_SZ:
        if (!(dwFlags & RRF_RT_REG_MULTI_SZ))
            return ERROR_UNSUPPORTED_TYPE;
        break;
    case REG_BINARY:
        if (!(dwFlags & RRF_RT_REG_BINARY))
            return ERROR_UNSUPPORTED_TYPE;
        break;
    default:
        break;
    }

    // like `RegGetValue()`: check for and append null-terminating characters
    if ((pData || pcbData) && (*pdwType == REG_SZ || *pdwType == REG_EXPAND_SZ || *pdwType == REG_MULTI_SZ)) {
        const DWORD nNullChars = *pdwType == REG_MULTI_SZ ? 2 : 1;
        if (!pData) {
            *pcbData = (*pcbData / sizeof(_TCHAR) + nNullChars) * sizeof(_TCHAR);
        }
        else // Append 'null-terminating' characters
        {
            _TCHAR* str = PTSTR(pData);
            for (DWORD nChars = *pcbData / sizeof(_TCHAR); nChars < nNullChars || str[nChars - nNullChars];
                 nChars = *pcbData / sizeof(_TCHAR)) {
                if (nChars >= cbBuf / sizeof(_TCHAR)) {
                    lRes = ERROR_MORE_DATA;
                    break;
                }
                else {
                    str[nChars] = _T('\0');
                    *pcbData = (*pcbData / sizeof(_TCHAR) + 1) * sizeof(_TCHAR);
                }
            }
        }
    }

    return lRes;
#else
    return RegGetValue(hKey, pszSubKey, pszValueName, dwFlags, pdwType, pData, pcbData);
#endif
}

static inline LSTATUS WTLSetKeyValueImpl(HKEY hKey, bool bWithSubKeyPaths, PCWSTR pszPath, DWORD dwType, LPCVOID pData, DWORD cbData)
{
    USES_ATL_SAFE_ALLOCA;
    _TCHAR* pszSubKey = NULL;
    const _TCHAR* pszValueName = pszPath;
    if (const _TCHAR* slashPos = bWithSubKeyPaths ? _tcsrchr(pszPath, _T('\\')) : NULL) {
        pszValueName = slashPos + 1;
        if (SIZE_T n = slashPos - pszPath) {
            pszSubKey = (PTSTR)_ATL_SAFE_ALLOCA((n + 1) * sizeof(_TCHAR), _ATL_SAFE_ALLOCA_DEF_THRESHOLD);
            if (!pszSubKey)
                return E_OUTOFMEMORY;
#ifdef _UNICODE
            wmemcpy(pszSubKey, pszPath, n);
#else
            memcpy(pszSubKey, pszPath, n);
#endif
            pszSubKey[n] = _T('\0');
        }
    }

#if (WTL_NTDDI_VERSION < NTDDI_VISTA)
    HKEY hkSub;
    if (LSTATUS lRes = RegCreateKeyEx(hKey, pszSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_SET_VALUE, NULL, &hkSub, NULL))
        return lRes;

    LSTATUS lRes = RegSetValueEx(hkSub, pszValueName, 0, dwType, PBYTE(pData), cbData);
    RegCloseKey(hkSub);
    hkSub = NULL;
    return lRes;
#else
    return RegSetKeyValue(hKey, pszSubKey, pszValueName, dwType, pData, cbData);
#endif
}

inline LSTATUS CRegKeyEx2::QueryValue(_In_opt_z_ PCTSTR pszPath, _Out_opt_ DWORD* pdwType, _Out_opt_ void* pData,
                                      _Inout_ ULONG* pnBytes) throw()
{
    ATLASSUME(m_hKey != NULL);

    return WTLQueryKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, RRF_RT_ANY | m_samWOW64, pdwType, pData, pnBytes);
}

inline LSTATUS CRegKeyEx2::QueryDWORDValue(_In_opt_z_ PCTSTR pszPath, _Out_ DWORD& dwValue) throw()
{
    ATLASSUME(m_hKey != NULL);

    DWORD nBytes = sizeof(DWORD);
    return WTLQueryKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, RRF_RT_DWORD | m_samWOW64, nullptr, &dwValue, &nBytes);
}

inline LSTATUS CRegKeyEx2::QueryQWORDValue(_In_opt_z_ PCTSTR pszPath, _Out_ ULONGLONG& qwValue) throw()
{
    ATLASSUME(m_hKey != NULL);

    DWORD nBytes = sizeof(ULONGLONG);
    return WTLQueryKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, RRF_RT_QWORD | m_samWOW64, nullptr, &qwValue, &nBytes);
}

inline LONG CRegKeyEx2::QueryBinaryValue(_In_opt_z_ PCTSTR pszPath, _Out_opt_ void* pValue, _Inout_opt_ ULONG* pnBytes) throw()
{
    ATLASSUME(m_hKey != NULL);
    WTLASSERT(pnBytes != NULL);

    return WTLQueryKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, RRF_RT_REG_BINARY | m_samWOW64, nullptr, pValue, pnBytes);
}

ATLPREFAST_SUPPRESS(6053)
/* prefast noise VSW 496818 */
inline LSTATUS CRegKeyEx2::QueryStringValue(_In_opt_z_ PCTSTR pszPath, _Out_writes_to_opt_(*pnChars, *pnChars) PTSTR pszValue,
                                            _Inout_ ULONG* pnChars) throw()
{
    ATLASSUME(m_hKey != NULL);
    WTLASSERT(pnChars != NULL);

    DWORD nBytes = (*pnChars) * sizeof(_TCHAR);
    *pnChars = 0;
    LSTATUS lRes = WTLQueryKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, RRF_RT_REG_SZ | RRF_RT_REG_EXPAND_SZ | m_samWOW64,
                                        nullptr, pszValue, &nBytes);

    if (lRes != ERROR_SUCCESS)
        return lRes;

    *pnChars = nBytes / sizeof(_TCHAR);

    return lRes;
}
ATLPREFAST_UNSUPPRESS()

ATLPREFAST_SUPPRESS(6053 6054 6385 6386)
/* prefast noise VSW 496818 */
inline LSTATUS CRegKeyEx2::QueryMultiStringValue(_In_opt_z_ PCTSTR pszPath, _Out_writes_to_opt_(*pnChars, *pnChars) PTSTR pszValue,
                                                 _Inout_ ULONG* pnChars) throw()
{
    ATLASSUME(m_hKey != NULL);
    WTLASSERT(pnChars != NULL);

    if (pszValue != NULL && *pnChars < 2)
        return ERROR_INSUFFICIENT_BUFFER;

    ULONG nBytes = (*pnChars) * sizeof(_TCHAR);
    *pnChars = 0;
    LSTATUS lRes =
        WTLQueryKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, RRF_RT_REG_MULTI_SZ | m_samWOW64, nullptr, pszValue, &nBytes);

    if (lRes != ERROR_SUCCESS)
        return lRes;

    *pnChars = nBytes / sizeof(_TCHAR);

    return lRes;
}
ATLPREFAST_UNSUPPRESS()

inline LSTATUS CRegKeyEx2::QueryGUIDValue(_In_opt_z_ PCTSTR pszPath, _Out_ GUID& guidValue) throw()
{
    _TCHAR szGUID[64];
    LONG lRes;
    ULONG nCount;
    HRESULT hr;

    ATLASSUME(m_hKey != NULL);

    guidValue = GUID_NULL;

    nCount = 64;
    lRes = QueryStringValue(pszPath, szGUID, &nCount);

    if (lRes != ERROR_SUCCESS)
        return lRes;

    if (szGUID[0] != _T('{'))
        return ERROR_INVALID_DATA;

    USES_CONVERSION_EX;
    LPOLESTR lpstr = T2OLE_EX(szGUID, _ATL_SAFE_ALLOCA_DEF_THRESHOLD);
#ifndef _UNICODE
    if (lpstr == NULL)
        return E_OUTOFMEMORY;
#endif

    hr = ::CLSIDFromString(lpstr, &guidValue);
    if (FAILED(hr))
        return ERROR_INVALID_DATA;

    return ERROR_SUCCESS;
}

inline LSTATUS CRegKeyEx2::SetValue(_In_opt_z_ PCTSTR pszPath, _In_ DWORD dwType, _In_opt_ const void* pValue,
                                    _In_ ULONG nBytes) throw()
{
    ATLASSUME(m_hKey != NULL);

    return WTLSetKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, dwType, pValue, nBytes);
}

inline LSTATUS CRegKeyEx2::SetBinaryValue(_In_opt_z_ PCTSTR pszPath, _In_opt_ const void* pData, _In_ ULONG nBytes) throw()
{
    ATLASSUME(m_hKey != NULL);

    return WTLSetKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, REG_BINARY, pData, nBytes);
}

inline LSTATUS CRegKeyEx2::SetDWORDValue(_In_opt_z_ PCTSTR pszPath, _In_ DWORD dwValue) throw()
{
    ATLASSUME(m_hKey != NULL);

    return WTLSetKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, REG_DWORD, &dwValue, sizeof(DWORD));
}

inline LSTATUS CRegKeyEx2::SetQWORDValue(_In_opt_z_ PCTSTR pszPath, _In_ ULONGLONG qwValue) throw()
{
    ATLASSUME(m_hKey != NULL);

    return WTLSetKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, REG_QWORD, &qwValue, sizeof(ULONGLONG));
}

inline LSTATUS CRegKeyEx2::SetStringValue(_In_opt_z_ PCTSTR pszPath, _In_opt_z_ PCTSTR pszValue, _In_ DWORD dwType,
                                          _In_ int nValueSize) throw()
{
    ATLASSUME(m_hKey != NULL);
    ATLENSURE_RETURN_VAL(pszValue != NULL, ERROR_INVALID_DATA);
    WTLASSERT((dwType == REG_SZ) || (dwType == REG_EXPAND_SZ));

    return WTLSetKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, dwType, pszValue,
                              DWORD((nValueSize == -1 ? (_tcslen(pszValue) + 1) : nValueSize) * sizeof(_TCHAR)));
}

inline LSTATUS CRegKeyEx2::SetMultiStringValue(_In_opt_z_ PCTSTR pszPath, _In_z_ PCTSTR pszValue) throw()
{
    PCTSTR pszTemp;
    ULONG nBytes;
    ULONG nLength;

    ATLASSUME(m_hKey != NULL);
    ATLENSURE_RETURN_VAL(pszValue != NULL, ERROR_INVALID_DATA);

    // Find the total length (in bytes) of all of the strings, including the
    // terminating '\0' of each string, and the second '\0' that terminates
    // the list.
    nBytes = 0;
    pszTemp = pszValue;
    do {
        nLength = static_cast<ULONG>(_tcslen(pszTemp)) + 1;
        pszTemp += nLength;
        nBytes += nLength * sizeof(_TCHAR);
    } while (nLength != 1);

    return WTLSetKeyValueImpl(m_hKey, m_bWithSubKeyPaths, pszPath, REG_MULTI_SZ, pszValue, nBytes);
}

inline LSTATUS CRegKeyEx2::SetGUIDValue(_In_opt_z_ PCTSTR pszPath, _In_ REFGUID guidValue) throw()
{
    OLECHAR szGUID[64];

    ATLASSUME(m_hKey != NULL);

    ATLENSURE_RETURN_VAL(::StringFromGUID2(guidValue, szGUID, 64), E_INVALIDARG);

    USES_CONVERSION_EX;
    PCTSTR pStr = OLE2CT_EX(szGUID, _ATL_SAFE_ALLOCA_DEF_THRESHOLD);
#ifndef _UNICODE
    if (pStr == NULL)
        return E_OUTOFMEMORY;
#endif
    return SetStringValue(pszPath, pStr);
}

} // namespace WTL

#ifndef _WTL_NO_AUTOMATIC_NAMESPACE
using namespace WTL;
#endif

#endif // __WTL_REGKEYEX_H__
